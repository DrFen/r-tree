﻿using System;

namespace Common.DAL.Exceptions
{
    /// <summary>
    /// Сущность не существует в репозитории
    /// </summary>
    public class NoEntityException : Exception
    {
        /// <summary>
        /// Исключение, сущность с таким идентификатором не найдена
        /// </summary>
        /// <param name="id"></param>
        public NoEntityException(Guid id ) : base($"entity with id {id} was not found") { }
    }
}
