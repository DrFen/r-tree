﻿using System;

namespace Common.DAL.Exceptions
{
    /// <summary>
    /// Ошибка уровня инициализации при тестировании. Z-индекс должен быть определён
    /// </summary>
    public class ZIndexMustBeDefinedException : Exception
    {
    }
}
