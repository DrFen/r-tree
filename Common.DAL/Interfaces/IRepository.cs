﻿using System;
using System.Collections.Generic;

namespace Common.DAL.Interfaces
{
    /// <summary>
    /// Интерфей работы репозитория
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity>
        where TEntity : class, IEntity
    {
        /// <summary>
        /// Получение сущности по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TEntity GetById(Guid id);

        /// <summary>
        ///  Получение отсортированной коллекции
        /// </summary>
        /// <param name="offset">Количество записей которые следует пропустить</param>
        /// <param name="pageSize">Размер возвращаемого блока</param>
        /// <returns></returns>
        IEnumerable<TEntity> GetSortedCollection(int? offset = null, int? pageSize =null);

        /// <summary>
        /// Добавление сущночти
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        TEntity Insert(TEntity entity);

        /// <summary>
        /// Обновление сущности
        /// </summary>
        /// <param name="newEntity"></param>
        /// <returns></returns>
        TEntity Update(TEntity newEntity);

        /// <summary>
        /// Удаление сущности
        /// </summary>
        /// <param name="id"></param>
        void Delete(Guid id);


        
    }
}
