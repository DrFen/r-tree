﻿using System;

namespace Common.DAL.Interfaces
{
    /// <summary>
    /// Абстрактная сущность для хранения
    /// </summary>
    public interface IEntity
    {
        Guid Id { get; }
    }
}
