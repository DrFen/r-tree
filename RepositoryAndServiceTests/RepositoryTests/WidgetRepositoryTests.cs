﻿using Common.DAL.Exceptions;
using Domain.WidgetDomains;
using InMemory.DAL.RepositoryRealization;
using System;
using System.Linq;
using Xunit;

namespace RepositoryAndServiceTests.RepositoryTests
{
    /// <summary>
    /// Тестирование репозитория в памяти
    /// </summary>
    public class WidgetRepositoryTests
    {

        private Widget[] Widgets => new[]
        {
            new Widget(Guid.Parse("00000000-0000-0000-0000-000000000001"), 1, 1, 1, 1, 1),
            new Widget(Guid.Parse("00000000-0000-0000-0000-000000000002"), 2, 2, 2, 1, 1),
            new Widget(Guid.Parse("00000000-0000-0000-0000-000000000003"), 3, 3, 3, 1, 1),
            new Widget(Guid.Parse("00000000-0000-0000-0000-000000000004"), 4, 4, 4, 1, 1),
            new Widget(Guid.Parse("00000000-0000-0000-0000-000000000005"), 5, 5, 5, 1, 1),
            new Widget(Guid.Parse("00000000-0000-0000-0000-000000000006"), 6, 6, 6, 1, 1)
        };

        #region init test
        /// <summary>
        /// Проверка вызова ошибок при передаче в конструтор коллекции содежащей элементы без z-индекса
        /// </summary>
        [Fact]
        public void FailInitializeWithNoZIndex()
        {
            var widgets = new[]
            {
                new Widget(0,0, null, 10, 10)
            };


            Assert.Throws<ZIndexMustBeDefinedException>(() =>
            {
                // ReSharper disable once ObjectCreationAsStatement
                new WidgetRepository(widgets);
            });

        }

        /// <summary>
        /// Успешное прохождение инициализации
        /// </summary>
        [Fact]
        public void SuccessInitialize()
        {
            var repository = new WidgetRepository(Widgets);
            Assert.Equal(6, repository.FromKeys.Count);
            Assert.Equal(6, repository.FromZIndex.Count);
            Assert.Equal(6, repository.FromTree.Count);
            CheckCoherency(repository, Guid.Parse("00000000-0000-0000-0000-000000000001"));
        }
        #endregion init test

        #region crud tests
        /// <summary>
        /// Успешная проверка получения по идентификатору
        /// </summary>
        [Fact]
        public void GetByIdSuccess()
        {
            var repository = new WidgetRepository(Widgets);
            var widget = repository.GetById(Guid.Parse("00000000-0000-0000-0000-000000000001"));
            Assert.Equal(1, widget.X);
            Assert.Equal(1, widget.Y);
            Assert.Equal(1, widget.Z);
        }

        /// <summary>
        /// Проверка исключения при попытке возвратить отсутствующую сущность
        /// </summary>
        [Fact]
        public void NoEntity()
        {
            var repository = new WidgetRepository(Widgets);
            Assert.Throws<NoEntityException>(() =>
            {
                repository.GetById(Guid.NewGuid());
            });
        }

        /// <summary>
        /// Проверка, что коллекция возвращается отсортированной по Z индексу 
        /// </summary>
        [Fact]
        public void GetSortedCollectionSuccess()
        {
            var currentZIndex = 1;
            new WidgetRepository(Widgets)
                .GetSortedCollection()
                .ToList()
                .ForEach(f =>
                {
                    Assert.Equal(currentZIndex, f.Z);
                    currentZIndex++;
                });

        }

        /// <summary>
        /// Проверка, что коллекция возвращается отсортированной по Z индексу с пейджингом
        /// </summary>
        [Fact]
        public void GetSortedCollectionWithPaging()
        {
            var currentZIndex = 4;
            new WidgetRepository(Widgets)
                .GetSortedCollection(3, 100)
                .ToList()
                .ForEach(f =>
                {
                    Assert.Equal(currentZIndex, f.Z);
                    currentZIndex++;
                });

        }

        /// <summary>
        /// Проверка согласованности внутренних коллекций
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="id"></param>
        private void CheckCoherency(WidgetRepository repository, Guid id)
        {
            var fromId = repository.FromKeys.Single(s => s.Id == id);
            var fromZIndex = repository.FromZIndex.Single(s => s.Id == id);
            var fromTree = repository.FromTree.Single(s => s.Id == id);
            Assert.True(fromId == fromTree && fromId == fromZIndex);
        }


        /// <summary>
        /// Добавление элемента с z индексом больше максимального
        /// </summary>
        [Fact]
        public void SuccessInsertWithZIndexUpperMaximum()
        {
            var repository = new WidgetRepository(Widgets);
            var widget = new Widget(1, 1, 9999, 1, 1);
            var id = widget.Id;
            repository.Insert(widget);
            CheckCoherency(repository, id);
            Assert.Equal(9999, repository.FromKeys.Single(s => s.Id == id).Z);
        }

        /// <summary>
        /// Добавление элемента с z индексом больше максимального
        /// </summary>
        [Fact]
        public void SuccessInsertWithZIndexInCenter()
        {
            var repository = new WidgetRepository(Widgets);
            var widget = new Widget(1, 1, 4, 1, 1);
            var id = widget.Id;
            repository.Insert(widget);

            CheckCoherency(repository, id);
            Assert.Equal(4, repository.FromKeys.Single(s => s.Id == id).Z);
            var resultCollection = repository.FromKeys;
            for (var i = 1; i <= 6; i++)
            {
                var checkId = Guid.Parse($"00000000-0000-0000-0000-00000000000{i}");
                CheckCoherency(repository, checkId);
                if (i < 4)
                {
                    Assert.Equal(i, resultCollection.Single(s => s.Id == checkId).Z);
                }
                else
                {
                    Assert.Equal(i + 1, resultCollection.Single(s => s.Id == checkId).Z);
                }
            }
        }

        /// <summary>
        /// Добавление элемента, когда в коллекции пусто, а z-index не определён
        /// </summary>
        [Fact]
        public void FirstInsertWithoutZ()
        {

            var repository = new WidgetRepository();
            var widget = new Widget(1, 1, null, 1, 1);
            var id = widget.Id;
            var result = repository.Insert(widget);
            Assert.Equal(1, result.Z);
            CheckCoherency(repository, id);
            Assert.Equal(1, repository.FromKeys.Single(s => s.Id == id).Z);
        }

        /// <summary>
        /// Изменение положения виджета
        /// </summary>
        [Fact]
        public void UpdateCoordinates()
        {
            var repository = new WidgetRepository(Widgets);
            var id = Guid.Parse("00000000-0000-0000-0000-000000000001");
            var newWidget = new Widget(id, 100, 200, 1, 300, 400);
            repository.Update(newWidget);
            CheckCoherency(repository, id);
            var fromRepo = repository.FromKeys.Single(s => s.Id == id);
            Assert.Equal(100, fromRepo.X);
            Assert.Equal(200, fromRepo.Y);
            Assert.Equal(300, fromRepo.Width);
            Assert.Equal(400, fromRepo.Height);
            Assert.Equal(1, fromRepo.Z);
        }

        /// <summary>
        /// Установка z индекса выше текущего максимума
        /// </summary>
        [Fact]
        public void UpdateZUpperMaximum()
        {
            var repository = new WidgetRepository(Widgets);
            var id = Guid.Parse("00000000-0000-0000-0000-000000000004");
            var newWidget = new Widget(id, 4, 4, 9999, 1, 1);
            repository.Update(newWidget);

            var resultCollection = repository.FromKeys;

            for (var i = 1; i <= 6; i++)
            {
                var checkId = Guid.Parse($"00000000-0000-0000-0000-00000000000{i}");
                CheckCoherency(repository, checkId);
                if (i != 4)
                {
                    Assert.Equal(i, resultCollection.Single(s => s.Id == checkId).Z);
                }
                else
                {
                    Assert.Equal(9999, resultCollection.Single(s => s.Id == checkId).Z);
                }
            }
        }

        /// <summary>
        /// Установка z индекса в текущем диапазоне
        /// </summary>
        [Fact]
        public void UpdateWithTransferZ()
        {
            var repository = new WidgetRepository(Widgets);
            var id = Guid.Parse("00000000-0000-0000-0000-000000000004");
            var newWidget = new Widget(id, 4, 4, 2, 1, 1);
            repository.Update(newWidget);

            var resultCollection = repository.FromKeys;

            for (var i = 1; i <= 6; i++)
            {
                var checkId = Guid.Parse($"00000000-0000-0000-0000-00000000000{i}");
                CheckCoherency(repository, checkId);

                switch (i)
                {
                    case 2:
                        Assert.Equal(3, resultCollection.Single(s => s.Id == checkId).Z);
                        break;
                    case 3:
                        Assert.Equal(4, resultCollection.Single(s => s.Id == checkId).Z);
                        break;
                    case 4:
                        Assert.Equal(2, resultCollection.Single(s => s.Id == checkId).Z);
                        break;
                    default:
                        Assert.Equal(i, resultCollection.Single(s => s.Id == checkId).Z);
                        break;

                }

            }
        }

        /// <summary>
        /// Установка z индекса в текущем диапазоне, в свободное место
        /// </summary>
        [Fact]
        public void UpdateWithTransferToEmptyPlace()
        {
            var repository = new WidgetRepository(Widgets.Where(w => w.Z != 3));
            var id = Guid.Parse("00000000-0000-0000-0000-000000000004");
            var newWidget = new Widget(id, 4, 4, 3, 1, 1);
            repository.Update(newWidget);

            var resultCollection = repository.FromKeys;

            for (var i = 1; i <= 6; i++)
            {
                if (i == 3)
                    continue;

                var checkId = Guid.Parse($"00000000-0000-0000-0000-00000000000{i}");
                CheckCoherency(repository, checkId);

                var entity = resultCollection.Single(s => s.Id == checkId);

                Assert.Equal(i == 4 ? 3 : i, entity.Z);
            }
        }

        /// <summary>
        /// Исключение при попытке обновить отсутствующую в репозитории сущность
        /// </summary>
        [Fact]
        public void NoEntityOnUpdate()
        {
            var repository = new WidgetRepository(Widgets);
            Assert.Throws<NoEntityException>(() =>
            {
                var newEntity = new Widget(Guid.NewGuid(), 1, 1, 1, 1, 1);
                repository.Update(newEntity);
            });
        }

        /// <summary>
        /// Проверка успешного прохождения удаления
        /// </summary>
        [Fact]
        public void SuccessDelete()
        {
            var repository = new WidgetRepository(Widgets);
            var id = Guid.Parse("00000000-0000-0000-0000-000000000004");
            repository.Delete(id);
            Assert.DoesNotContain(repository.FromKeys, w => w.Id == id);
            Assert.DoesNotContain(repository.FromTree, w => w.Id == id);
            Assert.DoesNotContain(repository.FromZIndex, w => w.Id == id);

            Assert.Equal(5, repository.FromKeys.Count);
            Assert.Equal(5, repository.FromTree.Count);
            Assert.Equal(5, repository.FromZIndex.Count);
        }

        /// <summary>
        /// Исключение при попытке удалить отсутствующую в репозитории сущность
        /// </summary>
        [Fact]
        public void NoEntityOnDelete()
        {
            var repository = new WidgetRepository(Widgets);
            Assert.Throws<NoEntityException>(() =>
            {
                repository.Delete(Guid.NewGuid());
            });
        }


        /// <summary>
        /// Получение только входящих в область виджетов
        /// </summary>
        [Fact]
        public void GetFilteredCollectionWithFiltration()
        {
            var currentZIndex = 1;
         
            var collection = new WidgetRepository(Widgets)
                .GetFilteredCollection(new WidgetRectangle(1, 1, 3, 3))
                .ToList();

            collection
                .ForEach(f =>
                {
                    Assert.Equal(currentZIndex, f.Z);
                    currentZIndex++;
                });

            Assert.Equal(3, collection.Count);
        }

        /// <summary>
        /// Проверка пустого диапазона
        /// </summary>
        [Fact]
        public void GetFilteredEmptyCollectionn()
        {
            var collection = new WidgetRepository(Widgets)
                .GetFilteredCollection(new WidgetRectangle(0, 0, 1, 1))
                .ToList();
           
            Assert.Empty(collection);
        }

        /// <summary>
        /// Получение только входящих в область виджетов, с заведомо большим диапазоном
        /// </summary>
        [Fact]
        public void GetFilteredFullCollection()
        {
            var currentZIndex = 1;

            var collection = new WidgetRepository(Widgets)
                .GetFilteredCollection(new WidgetRectangle(-10, -10, 300, 300))
                .ToList();

            collection
                .ForEach(f =>
                {
                    Assert.Equal(currentZIndex, f.Z);
                    currentZIndex++;
                });

            Assert.Equal(6, collection.Count);
        }
        #endregion crud tests

    }
}
