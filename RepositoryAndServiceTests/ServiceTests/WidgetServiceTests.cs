﻿using Common.DAL.Exceptions;
using Domain.WidgetDomains;
using Domain.WidgetRepositoryInterfaces;
using Moq;
using RTree.Extensions;
using Services.Requests;
using Services.Services;
using System;
using System.Linq;
using Xunit;

namespace RepositoryAndServiceTests.ServiceTests
{
    /// <summary>
    /// Тесты сервиса виджетов
    /// </summary>
    public class WidgetServiceTests
    {
        /// <summary>
        /// Тестирование создания в сервисе
        /// </summary>
        [Fact]
        public void CreateSuccess()
        {

            var mockRepository = new Mock<IWidgetRepository>();

            mockRepository
                .Setup(s => s.Insert(It.IsAny<Widget>()))
                .Returns<Widget>((widget) =>
                                    new Widget(Guid.NewGuid(), widget.X, widget.Y, widget.Z ?? 1, widget.Width, widget.Height));

            var service = new WidgetService(mockRepository.Object);

            var result = service.Create(new CreateWidgetRequest
            {
                X = 0,
                Y = 0,
                Height = 1,
                Width = 1
            });

            Assert.Equal(0, result.X);
            Assert.Equal(0, result.Y);
            Assert.Equal(1, result.Height);
            Assert.Equal(1, result.Width);
            Assert.Equal(1, result.ZIndex);
            Assert.NotEqual(Guid.Empty, result.Id);

            result = service.Create(new CreateWidgetRequest
            {
                X = 0,
                Y = 0,
                Height = 1,
                Width = 1,
                ZIndex = 100
            });

            Assert.Equal(0, result.X);
            Assert.Equal(0, result.Y);
            Assert.Equal(1, result.Height);
            Assert.Equal(1, result.Width);
            Assert.Equal(100, result.ZIndex);
            Assert.NotEqual(Guid.Empty, result.Id);
        }

        /// <summary>
        /// Тестирование получения по идентификатору
        /// </summary>
        [Fact]
        public void GetSuccess()
        {

            var mockRepository = new Mock<IWidgetRepository>();

            mockRepository
                .Setup(s => s.GetById(It.IsAny<Guid>()))
                .Returns<Guid>((identity) =>
                                    new Widget(identity, 1, 1, 1, 1, 1));

            var service = new WidgetService(mockRepository.Object);

            var id = Guid.NewGuid();
            var result = service.Get(new SimpleIdRequest
            {
                Id = id
            });

            Assert.Equal(1, result.X);
            Assert.Equal(1, result.Y);
            Assert.Equal(1, result.Height);
            Assert.Equal(1, result.Width);
            Assert.Equal(1, result.ZIndex);
            Assert.Equal(id, result.Id);
        }

        /// <summary>
        /// Тестирование проброса ошибки "нет виджета" при чтении
        /// </summary>
        [Fact]
        public void GetNoWidget()
        {
            var mockRepository = new Mock<IWidgetRepository>();

            mockRepository
                .Setup(s => s.GetById(It.IsAny<Guid>()))
                .Returns<Guid>((identity) => throw new NoEntityException(identity));

            var service = new WidgetService(mockRepository.Object);

            Assert.Throws<NoEntityException>(() => service.Get(new SimpleIdRequest
            {
                Id = Guid.NewGuid()
            }));
        }

        /// <summary>
        /// Успешное обновление виджета
        /// </summary>
        [Fact]
        public void UpdateSuccess()
        {
            var mockRepository = new Mock<IWidgetRepository>();

            mockRepository
                .Setup(s => s.Update(It.IsAny<Widget>()))
                .Returns<Widget>((widget) => widget.Clone());

            var service = new WidgetService(mockRepository.Object);
            var id = Guid.NewGuid();

            var result = service.Update(new UpdateWidgetRequest
            {
                Id = id,
                Height = 1,
                Width = 1,
                X = 1,
                Y = 1,
                ZIndex = 1
            });

            Assert.Equal(1, result.X);
            Assert.Equal(1, result.Y);
            Assert.Equal(1, result.Height);
            Assert.Equal(1, result.Width);
            Assert.Equal(1, result.ZIndex);
            Assert.Equal(id, result.Id);
        }

        /// <summary>
        /// Тестирование проброса ошибки "нет виджета" при обновлении
        /// </summary>
        [Fact]
        public void UpdateNoWidget()
        {
            var mockRepository = new Mock<IWidgetRepository>();

            mockRepository
                .Setup(s => s.Update(It.IsAny<Widget>()))
                .Returns<Widget>((widget) => throw new NoEntityException(widget.Id));

            var service = new WidgetService(mockRepository.Object);

            Assert.Throws<NoEntityException>(() => service.Update(new UpdateWidgetRequest
            {
                Id = Guid.NewGuid()
            }));
        }


        /// <summary>
        /// Успешное удаление виджета
        /// </summary>
        [Fact]
        public void DeleteSuccess()
        {
            var mockRepository = new Mock<IWidgetRepository>();

            mockRepository
                .Setup(s => s.Delete(It.IsAny<Guid>()))
                .Callback<Guid>((identity) => { })
                .Verifiable();

            var service = new WidgetService(mockRepository.Object);
            service.Delete(new SimpleIdRequest
            {
                Id = Guid.NewGuid()
            });
        }

        /// <summary>
        /// Тестирование проброса ошибки "нет виджета" при удалении
        /// </summary>
        [Fact]
        public void DeleteNoWidget()
        {
            var mockRepository = new Mock<IWidgetRepository>();

            mockRepository
                .Setup(s => s.Delete(It.IsAny<Guid>()))
                .Callback<Guid>((identity) => throw new NoEntityException(identity))
                .Verifiable();

            var service = new WidgetService(mockRepository.Object);

            Assert.Throws<NoEntityException>(() => service.Delete(new SimpleIdRequest
            {
                Id = Guid.NewGuid()
            }));
        }

        /// <summary>
        /// Получение спска виджетов без пэйджинга
        /// </summary>
        [Fact]
        public void SortedListWithoutPagingSuccess()
        {
            var mockRepository = new Mock<IWidgetRepository>();

            mockRepository
                .Setup(s => s.GetSortedCollection(It.IsAny<int?>(), It.IsAny<int?>()))
                .Returns(() => new[]
                {
                    new Widget(Guid.NewGuid(), 1,1,1,1,1),
                    new Widget(Guid.NewGuid(), 2,1,2,1,1),
                    new Widget(Guid.NewGuid(), 3,1,3,1,1),
                    new Widget(Guid.NewGuid(), 4,1,4,1,1),
                    new Widget(Guid.NewGuid(), 5,1,5,1,1),
                });

            var service = new WidgetService(mockRepository.Object);
            var collection = service.GetSortedList(new SimpleListRequest());

            var index = 0;
            collection.ToList()
                .ForEach(f =>
                {
                    index++;
                    Assert.Equal(index, f.ZIndex);
                    Assert.Equal(index, f.X);
                });
            Assert.Equal(5, index);

        }

        /// <summary>
        /// Получение списка виджетов с пэйджингом
        /// </summary>
        [Fact]
        public void SortedListWithPagingSuccess()
        {
            var mockRepository = new Mock<IWidgetRepository>();

            mockRepository
                .Setup(s => s.GetSortedCollection(It.IsAny<int?>(), It.IsAny<int?>()))
                .Returns(() => new[]
                {
                    new Widget(Guid.NewGuid(), 1,1,1,1,1),
                    new Widget(Guid.NewGuid(), 2,1,2,1,1),
                    new Widget(Guid.NewGuid(), 3,1,3,1,1),
                    new Widget(Guid.NewGuid(), 4,1,4,1,1),
                    new Widget(Guid.NewGuid(), 5,1,5,1,1),
                });

            var service = new WidgetService(mockRepository.Object);
            var collection = service.GetSortedList(new SimpleListRequest 
            {
                Offset = 0,
                PageSize = 10
            });

            var index = 0;
            collection.ToList()
                .ForEach(f =>
                {
                    index++;
                    Assert.Equal(index, f.ZIndex);
                    Assert.Equal(index, f.X);
                });

            Assert.Equal(5, index);

        }

        /// <summary>
        /// Получение списка виджетов без пэйджинга с отбором по области
        /// </summary>
        [Fact]
        public void FilteredListWithoutPagingSuccess()
        {
            var mockRepository = new Mock<IWidgetRepository>();

            mockRepository
                .Setup(s => s.GetFilteredCollection(It.IsAny<WidgetRectangle>(), It.IsAny<int?>(), It.IsAny<int?>()))
                .Returns(() => new[]
                {
                    new Widget(Guid.NewGuid(), 1,1,1,1,1),
                    new Widget(Guid.NewGuid(), 2,1,2,1,1),
                    new Widget(Guid.NewGuid(), 3,1,3,1,1),
                    new Widget(Guid.NewGuid(), 4,1,4,1,1),
                    new Widget(Guid.NewGuid(), 5,1,5,1,1),
                });

            var service = new WidgetService(mockRepository.Object);
            var collection = service.GetFilteredList(new FilteredListRequest 
            {
                X = 1,
                Y = 1,
                Height = 1,
                Width = 6                
            });

            var rect = new WidgetRectangle(1, 1, 6, 2);

            var index = 0;
            collection.ToList()
                .ForEach(f =>
                {
                    index++;
                    Assert.Equal(index, f.ZIndex);
                    Assert.Equal(index, f.X);
                    Assert.True(new WidgetRectangle(f.X, f.Y, f.Width, f.Height).RectangleInRectangle(rect));
                });
            Assert.Equal(5, index);

        }

        /// <summary>
        /// Получение списка виджетов с пэйджингом с отбором по области
        /// </summary>
        [Fact]
        public void FilteredListWithPagingSuccess()
        {
            var mockRepository = new Mock<IWidgetRepository>();

            mockRepository
                .Setup(s => s.GetFilteredCollection(It.IsAny<WidgetRectangle>(), It.IsAny<int?>(), It.IsAny<int?>()))
                .Returns(() => new[]
                {
                    new Widget(Guid.NewGuid(), 1,1,1,1,1),
                    new Widget(Guid.NewGuid(), 2,1,2,1,1),
                    new Widget(Guid.NewGuid(), 3,1,3,1,1),
                    new Widget(Guid.NewGuid(), 4,1,4,1,1),
                    new Widget(Guid.NewGuid(), 5,1,5,1,1),
                });

            var service = new WidgetService(mockRepository.Object);
            var collection = service.GetFilteredList(new FilteredListRequest
            {
                X = 1,
                Y = 1,
                Height = 1,
                Width = 6,
                Offset = 0,
                PageSize = 6
            });

            var rect = new WidgetRectangle(1, 1, 6, 2);

            var index = 0;
            collection.ToList()
                .ForEach(f =>
                {
                    index++;
                    Assert.Equal(index, f.ZIndex);
                    Assert.Equal(index, f.X);
                    Assert.True(new WidgetRectangle(f.X, f.Y, f.Width, f.Height).RectangleInRectangle(rect));
                });
            Assert.Equal(5, index);

        }
    }
}
