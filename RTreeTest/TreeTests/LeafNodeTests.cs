﻿using System;
using System.Linq;
using System.Reflection;
using RTree;
using RTree.GeometryHelpers;
using RTree.TreeRealization;
using RTreeTest.Wrappers.AbstractClassWrappers;
using Xunit;

namespace RTreeTest.TreeTests
{
    public class LeafNodeTests
    {

        /// <summary>
        /// Нижняя левая граница узла
        /// </summary>
        private Point LeftLower => new Point(0, 0);

        /// <summary>
        /// Верхняя правая граница узла
        /// </summary>
        private Point RightUpper => new Point(10, 10);

        private double InvokeGetDelimiter(LeafNode<Point> instance, out int leftCount, bool isX = true)
        {
            var method = typeof(LeafNode<Point>).GetMethod($"Get{(isX ? "X" : "Y")}Delimiter", BindingFlags.NonPublic | BindingFlags.Instance);
            leftCount = 0;
            var args = new object[] { leftCount };
            var result = (double)(method?.Invoke(instance, args) ?? 0);
            leftCount = (int)args[0];
            return result;
        }

        private TreeNode<Point> InvokeSplit(LeafNode<Point> instance)
        {
            return typeof(LeafNode<Point>).GetMethod("Split", BindingFlags.NonPublic | BindingFlags.Instance)?
                .Invoke(instance, new object[0]) as TreeNode<Point>;

        }

        /// <summary>
        /// Поиск разделителя Успешное прохождение при равномерном распределении
        /// </summary>
        [Theory]
        [InlineData(false)]
        [InlineData(true)]
        public void GetDelimiterSuccess(bool isXCoordinates)
        {
            var points = new[]
            {
                new Point(0,0),
                new Point(2,2),
                new Point(4,4),
                new Point(6,6),
                new Point(8,8),
            };

            var leaf = new LeafNode<Point>(LeftLower, RightUpper, null, points);
            var delimiter = InvokeGetDelimiter(leaf, out var leftCount, isXCoordinates);

            Assert.Equal(2, leftCount);
            Assert.Equal(2, delimiter);
        }



        /// <summary>
        /// Больше половины точек находятся в одних координатах
        /// </summary>
        [Theory]
        [InlineData(false)]
        [InlineData(true)]
        public void MutiplePointsWithSameCoordinates(bool isXCoordinates)
        {
            var points = new[]
            {
                new Point(2,2),
                new Point(2,2),
                new Point(2,2),
                new Point(2,2),
                new Point(8,8),
            };

            var leaf = new LeafNode<Point>(LeftLower, RightUpper, null, points);
            var delimiter = InvokeGetDelimiter(leaf, out var leftCount, isXCoordinates);

            Assert.Equal(4, leftCount);
            Assert.Equal(2, delimiter);
        }

        /// <summary>
        /// Больше половины точек находятся в нулевых координатах
        /// </summary>
        [Theory]
        [InlineData(false)]
        [InlineData(true)]
        public void MutiplePointsWithZeroCoordinates(bool isXCoordinates)
        {
            var points = new[]
            {
                new Point(0,0),
                new Point(0,0),
                new Point(0,0),
                new Point(0,0),
                new Point(8,8),
            };

            var leaf = new LeafNode<Point>(LeftLower, RightUpper, null, points);
            var delimiter = InvokeGetDelimiter(leaf, out var leftCount, isXCoordinates);

            Assert.Equal(4, leftCount);
            Assert.Equal(1, delimiter);
        }


        /// <summary>
        /// Все точки находятся в нулевых координатах
        /// </summary>
        [Theory]
        [InlineData(false)]
        [InlineData(true)]
        public void AllPointsWithZeroCoordinates(bool isXCoordinates)
        {
            var points = new[]
            {
                new Point(0,0),
                new Point(0,0),
                new Point(0,0),
                new Point(0,0),
                new Point(0,0),
            };

            var leaf = new LeafNode<Point>(LeftLower, RightUpper, null, points);
            var delimiter = InvokeGetDelimiter(leaf, out var leftCount, isXCoordinates);

            Assert.Equal(5, leftCount);
            Assert.Equal(0, delimiter);
        }

        /// <summary>
        /// Все точки находятся в одинаковых координатах
        /// </summary>
        [Theory]
        [InlineData(false)]
        [InlineData(true)]
        public void AllPointsWithSameCoordinates(bool isXCoordinates)
        {
            var points = new[]
            {
                new Point(6,6),
                new Point(6,6),
                new Point(6,6),
                new Point(6,6),
                new Point(6,6),
            };

            var leaf = new LeafNode<Point>(LeftLower, RightUpper, null, points);
            var delimiter = InvokeGetDelimiter(leaf, out var leftCount, isXCoordinates);

            Assert.Equal(5, leftCount);
            Assert.Equal(6, delimiter);
        }

        /// <summary>
        /// Проверка разделения по оси X с рамномерным распределением
        /// </summary>
        [Fact]
        public void SuccessSplitX()
        {
            var points = new[]
            {
                new Point(2,6),
                new Point(4,6),
                new Point(6,6),
                new Point(8,6),
                new Point(9,6)
            };

            var leaf = new LeafNode<Point>(LeftLower, RightUpper, null, points);
            var node = InvokeSplit(leaf) as HorizontalTreeNode<Point>;
            Assert.NotNull(node);
            Assert.Equal(0, node.LeftLower.X);
            Assert.Equal(0, node.LeftLower.Y);
            Assert.Equal(10, node.RightUpper.X);
            Assert.Equal(10, node.RightUpper.Y);


            Assert.Equal(0, node.LeftNode.LeftLower.X);
            Assert.Equal(0, node.LeftNode.LeftLower.Y);
            Assert.Equal(4, node.LeftNode.RightUpper.X);
            Assert.Equal(10, node.LeftNode.RightUpper.Y);
            Assert.Equal(2, (node.LeftNode as LeafNode<Point>)?.Points.Count);

            Assert.Contains((node.LeftNode as LeafNode<Point>)?.Points ?? throw new InvalidOperationException(),
                point => point.Value.X.Equals(2));

            Assert.Contains(((LeafNode<Point>) node.LeftNode).Points ?? throw new InvalidOperationException(),
                point => point.Value.X.Equals(4));


            Assert.Equal(4, node.RightNode.LeftLower.X);
            Assert.Equal(0, node.RightNode.LeftLower.Y);
            Assert.Equal(10, node.RightNode.RightUpper.X);
            Assert.Equal(10, node.RightNode.RightUpper.Y);
            Assert.Equal(3, (node.RightNode as LeafNode<Point>)?.Points.Count);


            Assert.Contains((node.RightNode as LeafNode<Point>)?.Points ?? throw new InvalidOperationException(),
                point => point.Value.X.Equals(6));

            Assert.Contains(((LeafNode<Point>) node.RightNode).Points ?? throw new InvalidOperationException(),
                point => point.Value.X.Equals(8));

            Assert.Contains(((LeafNode<Point>) node.RightNode).Points ?? throw new InvalidOperationException(),
                point => point.Value.X.Equals(9));
        }


        /// <summary>
        /// Проверка разделения по оси Y с рамномерным распределением
        /// </summary>
        [Fact]
        public void SuccessSplitY()
        {
            var points = new[]
            {
                new Point(6, 2),
                new Point(6, 4),
                new Point(6, 6),
                new Point(6, 8),
                new Point(6, 9)
            };

            var leaf = new LeafNode<Point>(LeftLower, RightUpper, null, points);
            var node = InvokeSplit(leaf) as VerticalTreeNode<Point>;
            Assert.NotNull(node);
            Assert.Equal(0, node.LeftLower.X);
            Assert.Equal(0, node.LeftLower.Y);
            Assert.Equal(10, node.RightUpper.X);
            Assert.Equal(10, node.RightUpper.Y);


            Assert.Equal(0, node.LeftNode.LeftLower.X);
            Assert.Equal(0, node.LeftNode.LeftLower.Y);
            Assert.Equal(10, node.LeftNode.RightUpper.X);
            Assert.Equal(4, node.LeftNode.RightUpper.Y);
            Assert.Equal(2, (node.LeftNode as LeafNode<Point>)?.Points.Count);
            Assert.Contains((node.LeftNode as LeafNode<Point>)?.Points ?? throw new InvalidOperationException(),
                point => point.Value.Y.Equals(2));

            Assert.Contains(((LeafNode<Point>) node.LeftNode).Points ?? throw new InvalidOperationException(),
                point => point.Value.Y.Equals(4));

            Assert.Equal(0, node.RightNode.LeftLower.X);
            Assert.Equal(4, node.RightNode.LeftLower.Y);
            Assert.Equal(10, node.RightNode.RightUpper.X);
            Assert.Equal(10, node.RightNode.RightUpper.Y);
            Assert.Equal(3, (node.RightNode as LeafNode<Point>)?.Points.Count);

            Assert.Contains((node.RightNode as LeafNode<Point>)?.Points ?? throw new InvalidOperationException(),
                point => point.Value.Y.Equals(6));

            Assert.Contains(((LeafNode<Point>) node.RightNode).Points ?? throw new InvalidOperationException(),
                point => point.Value.Y.Equals(8));

            Assert.Contains(((LeafNode<Point>) node.RightNode).Points ?? throw new InvalidOperationException(),
                point => point.Value.Y.Equals(9));
        }

        /// <summary>
        /// Проверка, невозможности разделения
        /// </summary>
        [Fact]
        public void NoSplitDecision()
        {
            var points = new[]
            {
                new Point(6, 6),
                new Point(6, 6),
                new Point(6, 6),
                new Point(6, 6),
                new Point(6, 6)
            };

            var leaf = new LeafNode<Point>(LeftLower, RightUpper, null, points);
            var node = InvokeSplit(leaf);
            Assert.Null(node);
        }




        /// <summary>
        /// Проврека добавления точек
        /// </summary>
        [Fact]
        public void SuccessAdding()
        {

            var leaf = new LeafNode<Point>(LeftLower, RightUpper, null, new[] { new Point(1, 1) });

            var root = RootNode<Point>.Register(leaf);
            Assert.NotNull(root.Child as LeafNode<Point>);

            Assert.Equal(0, leaf.LeftLower.X);
            Assert.Equal(0, leaf.LeftLower.Y);
            Assert.Equal(10, leaf.RightUpper.X);
            Assert.Equal(10, leaf.RightUpper.Y);
            Assert.Single(leaf.Points);
            Assert.Contains(leaf.Points, point => point.Value.Y.Equals(1));


            leaf.AddPoint(new Point(20, 20));
            leaf.AddPoint(new Point(30, -30));
            leaf.AddPoint(new Point(-40, 40));
            leaf.AddPoint(new Point(50, 50));

            Assert.Equal(-40, leaf.LeftLower.X);
            Assert.Equal(-30, leaf.LeftLower.Y);
            Assert.Equal(50, leaf.RightUpper.X);
            Assert.Equal(50, leaf.RightUpper.Y);
            Assert.Equal(5, leaf.Points.Count);
            Assert.Contains(leaf.Points, point => point.Value.Y.Equals(1));
            Assert.Contains(leaf.Points, point => point.Value.Y.Equals(20));
            Assert.Contains(leaf.Points, point => point.Value.Y.Equals(-30));
            Assert.Contains(leaf.Points, point => point.Value.Y.Equals(40));
            Assert.Contains(leaf.Points, point => point.Value.Y.Equals(50));

            leaf.AddPoint(new Point(60, 60));

            Assert.NotNull(root.Child as TreeNode<Point>);
            Assert.Equal(-40, root.LeftLower.X);
            Assert.Equal(-30, root.LeftLower.Y);
            Assert.Equal(60, root.RightUpper.X);
            Assert.Equal(60, root.RightUpper.Y);

            Assert.Equal(2, ((root.Child as TreeNode<Point>)?.LeftNode as LeafNode<Point>)?.Points.Count);
            Assert.Equal(4, ((root.Child as TreeNode<Point>)?.RightNode as LeafNode<Point>)?.Points.Count);
        }

        /// <summary>
        /// Рекурсивное получение всех точек
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private Point[] GetAllPoints(NodeAbstract<Point> node)
        {
            Point[] result = null;
            if (node.NodeType == NodeType.Leaf)
            {
                result = (node as LeafNode<Point>)?.Points.Select(s => s.Value).ToArray();
            }

            if (node.NodeType == NodeType.TreeNode)
                result = GetAllPoints((node as TreeNode<Point>)?.LeftNode)
                    .Concat(GetAllPoints((node as TreeNode<Point>)?.RightNode))
                    .ToArray();

            if (result == null)
                throw new NotImplementedException();

            CheckAllPointsInArea(node, result);
            return result;
        }


        /// <summary>
        /// Проверка, что все точки принадлежат фигуре
        /// </summary>
        /// <param name="node"></param>
        /// <param name="points"></param>
        private void CheckAllPointsInArea(NodeAbstract<Point> node, Point[] points)
        {
            foreach (var point in points)
            {
                Assert.True(IncludeHelper.PointInNode(point, node));
            }
        }

        /// <summary>
        /// Комплексная проверка
        /// </summary>
        [Fact]
        public void ComplexTest()
        {
            var firstLeaf = new LeafNode<Point>(new Point(0, 0));
            var root = RootNode<Point>.Register(firstLeaf);

            for (var i = 1; i <= 99; i++)
            {
                root.AddPoint(new Point(i, i));
            }

            Assert.Equal(0, root.LeftLower.X);
            Assert.Equal(0, root.LeftLower.Y);
            Assert.Equal(99, root.RightUpper.X);
            Assert.Equal(99, root.RightUpper.Y);

            Assert.Equal(100, GetAllPoints(root.Child).Length);
        }

        /// <summary>
        /// Получение листового узла с координатами 0;0 10;10 с 5 точками
        /// </summary>
        /// <returns></returns>
        public static LeafNode<Point> GetLeafNode()
        {
            var points = new[]
           {
                new Point(-5 , -5),
                new Point(-5, 5),
                new Point(5, 5),
                new Point(5, -5),
                new Point(0, 0),
            };

            return new LeafNode<Point>(new Point(-10, -10), new Point(10, 10), null, points);
        }

        /// <summary>
        /// Проверка выдачи дочек находящихся внутри листа дерева
        /// </summary>
        /// <param name="leftX">Координаты левого нижнего угла прямоугольника по оси X</param>
        /// <param name="leftY">Координаты левого нижнего угла прямоугольника по оси Y</param>
        /// <param name="rightX">Координаты правого верхнего угла прямоугольника по оси X</param>
        /// <param name="rightY">Координаты правого верхнего угла прямоугольника по оси Y</param>
        /// <param name="count">Ожидаемое количество дочерних узлов</param>
        [Theory]
        //empty
        [InlineData(-11, -11, -8, 11, 0)]
        [InlineData(-100, -100, -50, -50, 0)]
        [InlineData(1, 1, 2, 2, 0)]
        //only one
        [InlineData(-6, -6, -4, -4, 1)]
        [InlineData(-6, -6, -5, -5, 1)]
        [InlineData(-1, -1, 1, 1, 1)]
        //all
        [InlineData(-100, -100, 100, 100, 5)]
        [InlineData(-9, -9, 9, 9, 5)]
        //multiple
        [InlineData(-9, -9, 1, 10, 3)]
        [InlineData(-9, -9, 10, 1, 3)]
        public void CheckGetPointInRectangle(double leftX,
                                            double leftY,
                                            double rightX,
                                            double rightY,
                                            int count)
        {

            var rectangle = new RectangeWrapper(new Point(leftX, leftY), new Point(rightX, rightY));
            var leafNode = GetLeafNode();
            Assert.Equal(count, leafNode.GetPointsInRectangle(rectangle, null).Count);
        }


        /// <summary>
        /// Проверка получения с дополнительным отбором
        /// </summary>
        [Fact]
        public void CheckGetPointInRectangleWithWhereClause()
        {

            var rectangle = new RectangeWrapper(new Point(-9, -9), new Point(10, 1));
            var leafNode = GetLeafNode();
            Assert.Equal(2, leafNode.GetPointsInRectangle(rectangle, (keyPoint) => !keyPoint.Value.X.Equals(0)).Count);
        }

        /// <summary>
        /// Проверка удаления
        /// </summary>
        [Fact]
        public void DeleteTest()
        {
            var leafNode = GetLeafNode();
            var point = leafNode.Points.Single(s => s.Value.X.Equals(5) && s.Value.Y.Equals(5));

            Assert.Equal(point.Key, point.Value.Id);
            Assert.Equal(5, leafNode.Points.Count);
            leafNode.DeletePoint(point.Key);
            Assert.Equal(4, leafNode.Points.Count);
            Assert.False(leafNode.Points.ContainsKey(point.Key));
        }
    }
}
