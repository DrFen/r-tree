﻿using System;
using RTree;
using RTree.TreeRealization;
using RTreeTest.Wrappers.AbstractClassWrappers;
using System.Linq;
using RTree.Extensions;
using Xunit;

namespace RTreeTest.TreeTests
{
    /// <summary>
    /// Тесты корневого элемента дерева
    /// </summary>
    public class RootNodeTests
    {
        /// <summary>
        /// Проверка выдачи точек находящихся внутри единственного листа корня дерева
        /// </summary>
        /// <param name="leftX">Координаты левого нижнего угла прямоугольника по оси X</param>
        /// <param name="leftY">Координаты левого нижнего угла прямоугольника по оси Y</param>
        /// <param name="rightX">Координаты правого верхнего угла прямоугольника по оси X</param>
        /// <param name="rightY">Координаты правого верхнего угла прямоугольника по оси Y</param>
        /// <param name="count">Ожидаемое количество дочерних узлов</param>
        [Theory]
        //empty
        [InlineData(-11, -11, -8, 11, 0)]
        [InlineData(-100, -100, -50, -50, 0)]
        [InlineData(1, 1, 2, 2, 0)]
        //only one
        [InlineData(-6, -6, -4, -4, 1)]
        [InlineData(-6, -6, -5, -5, 1)]
        [InlineData(-1, -1, 1, 1, 1)]
        //all
        [InlineData(-100, -100, 100, 100, 5)]
        [InlineData(-9, -9, 9, 9, 5)]
        //multiple
        [InlineData(-9, -9, 1, 10, 3)]
        [InlineData(-9, -9, 10, 1, 3)]
        public void OnlyOneLeaf(double leftX,
                                double leftY,
                                double rightX,
                                double rightY,
                                int count)
        {
            var rectangle = new RectangeWrapper(new Point(leftX, leftY), new Point(rightX, rightY));
            var rootNode = RootNode<Point>.Register(LeafNodeTests.GetLeafNode());
            Assert.Equal(count, rootNode.GetPointsInRectangle(rectangle).Count);

        }

        /// <summary>
        /// Проверка удаления с единственным листом
        /// </summary>
        [Fact]
        public void DeletePoint()
        {
            var leafNode = LeafNodeTests.GetLeafNode();
            var rootNode = RootNode<Point>.Register(leafNode);
            var point = leafNode.Points.Single(s => s.Value.X.Equals(5) && s.Value.Y.Equals(5));

            Assert.Equal(point.Key, point.Value.Id);
            Assert.Equal(5, leafNode.Points.Count);
            rootNode.DeletePoint(point.Value);
            Assert.Equal(4, leafNode.Points.Count);
            Assert.False(leafNode.Points.ContainsKey(point.Key));

        }

        /// <summary>
        /// Контроль изменения границ при росте точек
        /// </summary>
        [Fact]
        public void MultiplePointAdd()
        {
            var leafNode = new LeafNode<Point>(new Point(0, 0));
            var rootNode = RootNode<Point>.Register(leafNode);

            Assert.Equal(0, rootNode.LeftUpperX);
            Assert.Equal(0, rootNode.LeftUpperY);
            Assert.Equal(0, rootNode.RightLowerX);
            Assert.Equal(0, rootNode.RightLowerY);

            for (var i = 1; i < 100; i++)
            {
                rootNode.InsertPoint(new Point(i, i));
                Assert.Equal(0, rootNode.LeftUpperX);
                Assert.Equal(i, rootNode.LeftUpperY);
                Assert.Equal(i, rootNode.RightLowerX);
                Assert.Equal(0, rootNode.RightLowerY);
            }

            for (var i = -1; i < -100; i--)
            {
                rootNode.InsertPoint(new Point(i, i));
                Assert.Equal(i, rootNode.LeftUpperX);
                Assert.Equal(99, rootNode.LeftUpperY);
                Assert.Equal(99, rootNode.RightLowerX);
                Assert.Equal(i, rootNode.RightLowerY);
            }
        }


        [Fact]
        public void Complex()
        {

            var leafNode = new LeafNode<Point>(new Point(-5, -5));
            var rootNode = RootNode<Point>.Register(leafNode);
            var id = Guid.NewGuid();
            var forDelete = new Point(id, -2, -2);


            rootNode.AddPoint(new Point(-5, -5));
            Assert.Equal(-5, rootNode.Child.LeftLower.X);
            Assert.Equal(-5, rootNode.Child.LeftLower.Y);
            Assert.Equal(-5, rootNode.Child.RightUpper.X);
            Assert.Equal(-5, rootNode.Child.RightUpper.Y);


            rootNode.AddPoint(new Point(-5, 5));

            Assert.Equal(-5, rootNode.Child.LeftLower.X);
            Assert.Equal(-5, rootNode.Child.LeftLower.Y);
            Assert.Equal(-5, rootNode.Child.RightUpper.X);
            Assert.Equal(5, rootNode.Child.RightUpper.Y);

            rootNode.AddPoint(new Point(5, 5));

            Assert.Equal(-5, rootNode.Child.LeftLower.X);
            Assert.Equal(-5, rootNode.Child.LeftLower.Y);
            Assert.Equal(5, rootNode.Child.RightUpper.X);
            Assert.Equal(5, rootNode.Child.RightUpper.Y);

            rootNode.AddPoint(new Point(5, -5));

            Assert.Equal(-5, rootNode.Child.LeftLower.X);
            Assert.Equal(-5, rootNode.Child.LeftLower.Y);
            Assert.Equal(5, rootNode.Child.RightUpper.X);
            Assert.Equal(5, rootNode.Child.RightUpper.Y);
            Assert.True(rootNode.Child.RectangleInRectangle(new RectangeWrapper(new Point(-5, -5), new Point(5, 5))));


            rootNode.AddPoint(forDelete);
            rootNode.AddPoint(new Point(-2, 2));
            rootNode.AddPoint(new Point(2, 2));
            rootNode.AddPoint(new Point(2, -2));

            Assert.True(rootNode.Child.RectangleInRectangle(new RectangeWrapper(new Point(-5, -5), new Point(5, 5))));
            Assert.Equal(-5, rootNode.Child.LeftLower.X);
            Assert.Equal(-5, rootNode.Child.LeftLower.Y);
            Assert.Equal(5, rootNode.Child.RightUpper.X);
            Assert.Equal(5, rootNode.Child.RightUpper.Y);

            Assert.Equal(9, rootNode.GetPointsInRectangle(new RectangeWrapper(new Point(-5, -5), new Point(5, 5))).Count);

            rootNode.DeletePoint(forDelete);

            Assert.True(rootNode.Child.RectangleInRectangle(new RectangeWrapper(new Point(-5, -5), new Point(5, 5))));

            Assert.Equal(8, rootNode.GetPointsInRectangle(new RectangeWrapper(new Point(-5, -5), new Point(5, 5))).Count);
            Assert.Equal(4, rootNode.GetPointsInRectangle(new RectangeWrapper(new Point(-5, -5), new Point(1, 5))).Count);



        }
    }
}
