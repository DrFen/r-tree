﻿using System;
using System.Collections.Generic;
using RTree;
using RTree.TreeRealization;
using RTreeTest.Wrappers.AbstractClassWrappers;
using Xunit;

namespace RTreeTest.TreeTests
{
    /// <summary>
    /// Тестирование общих механизмов TreeNode
    /// </summary>
    public class TreeNodeTest
    {
        /// <summary>
        /// Проверка выдачи валидных узлов входящих в прямоугольник
        /// </summary>
        /// <param name="leftX">Координаты левого нижнего угла прямоугольника по оси X</param>
        /// <param name="leftY">Координаты левого нижнего угла прямоугольника по оси Y</param>
        /// <param name="rightX">Координаты правого верхнего угла прямоугольника по оси X</param>
        /// <param name="rightY">Координаты правого верхнего угла прямоугольника по оси Y</param>
        /// <param name="count">Ожидаемое количество дочерних узлов</param>
        /// <param name="resultCoordinates">Если узел один, охидаемый результат координат левого нижнего угла по оси X</param>
        [Theory]
        //All
        [InlineData(-11, -11, 40, 40, 2, null)]
        //Only left
        [InlineData(-11, 9, -9, 11, 1, -10)]
        [InlineData(-11, 0, -9, 1, 1, -10)]
        [InlineData(-11, -11, -9, -9, 1, -10)]
        [InlineData(0, 0, 1, 1, 1, -10)]
        //Left and right intersection
        [InlineData(9, 9, 11, 11, 2, null)]
        [InlineData(9, 0, 11, 1, 2, null)]
        [InlineData(9, -11, 11, -9, 2, null)]
        //Only right
        [InlineData(29, 9, 31, 11, 1, 10)]
        [InlineData(29, 0, 31, 1, 1, 10)]
        [InlineData(29, -11, 31, -9, 1, 10)]
        [InlineData(20, 0, 21, 1, 1, 10)]
        

        public void GetNodesInRectangle(double leftX,
                                        double leftY,
                                        double rightX,
                                        double rightY,
                                        int count,
                                        double? resultCoordinates)
        {


            var firstNode = new LeafNode<Point>(new Point(-10, -10), new Point(10, 10), null, new Point[0]);
            var secondNode = new LeafNode<Point>(new Point(10, -10), new Point(30, 10), null, new Point[0]);
            var treeNode = new HorizontalTreeNode<Point>(firstNode, secondNode, null, new List<KeyValuePair<Guid, Point>>());

            var result = treeNode.GetNodesInRectangle(new RectangeWrapper(new Point(leftX, leftY), new Point(rightX, rightY)));
            Assert.Equal(count, result.Count);

            if (resultCoordinates != null)
                Assert.Single(result, point => point.LeftLower.X.Equals(resultCoordinates));


        }
    }
}
