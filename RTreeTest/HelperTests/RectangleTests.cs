﻿using RTree;
using RTree.GeometryHelpers;
using RTreeTest.Wrappers.AbstractClassWrappers;
using Xunit;

namespace RTreeTest.HelperTests
{
    /// <summary>
    /// Тесты на поведение прямоугольника
    /// </summary>
    public class RectangleTests
    {
        /// <summary>
        /// Получение прямоугольника с масштабированием его до координат
        /// </summary>
        /// <param name="x">Координата по оси X</param>
        /// <param name="y">Координата по оси Y</param>
        /// <returns></returns>
        private RectangeWrapper GetRectangleWithResize(double x, double y)
        {
            var result = new RectangeWrapper(new Point(0, 0), new Point(10, 10));
            result.IncreaseSizeWrapped(new Point(x, y));
            return result;
        }

        /// <summary>
        /// Проверка неизменности прямоугольника при попытке расширить до принадлежащей ему точке
        /// </summary>
        [Fact]
        public void PointInRectangle()
        {
            var rectangle = GetRectangleWithResize(5, 5);
            Assert.Equal(0, rectangle.LeftLower.X);
            Assert.Equal(0, rectangle.LeftLower.Y);
            Assert.Equal(10, rectangle.RightUpper.X);
            Assert.Equal(10, rectangle.RightUpper.Y);
        }

        /// <summary>
        /// Проверка расширения налево
        /// </summary>
        [Fact]
        public void LeftResize()
        {
            var rectangle = GetRectangleWithResize(-1, 5);
            rectangle.IncreaseSizeWrapped(new Point(-1, 5));
            Assert.Equal(-1, rectangle.LeftLower.X);
            Assert.Equal(0, rectangle.LeftLower.Y);
            Assert.Equal(10, rectangle.RightUpper.X);
            Assert.Equal(10, rectangle.RightUpper.Y);
        }

        /// <summary>
        /// Проверка расширения направо
        /// </summary>
        [Fact]
        public void RightResize()
        {
            var rectangle = GetRectangleWithResize(11, 5);
            Assert.Equal(0, rectangle.LeftLower.X);
            Assert.Equal(0, rectangle.LeftLower.Y);
            Assert.Equal(11, rectangle.RightUpper.X);
            Assert.Equal(10, rectangle.RightUpper.Y);
        }

        /// <summary>
        /// Проверка расширения вверх
        /// </summary>
        [Fact]
        public void UpResize()
        {
            var rectangle = GetRectangleWithResize(0, 11);
            Assert.Equal(0, rectangle.LeftLower.X);
            Assert.Equal(0, rectangle.LeftLower.Y);
            Assert.Equal(10, rectangle.RightUpper.X);
            Assert.Equal(11, rectangle.RightUpper.Y);
        }

        /// <summary>
        /// Проверка расширения вниз
        /// </summary>
        [Fact]
        public void DownResize()
        {
            var rectangle = GetRectangleWithResize(0, -1);
            Assert.Equal(0, rectangle.LeftLower.X);
            Assert.Equal(-1, rectangle.LeftLower.Y);
            Assert.Equal(10, rectangle.RightUpper.X);
            Assert.Equal(10, rectangle.RightUpper.Y);
        }

        /// <summary>
        /// Проверка расширения по 2 измерениям
        /// </summary>
        [Fact]
        public void ComplexResize()
        {
            var rectangle = GetRectangleWithResize(-1, -2);
            Assert.Equal(-1, rectangle.LeftLower.X);
            Assert.Equal(-2, rectangle.LeftLower.Y);
            Assert.Equal(10, rectangle.RightUpper.X);
            Assert.Equal(10, rectangle.RightUpper.Y);
        }


        /// <summary>
        /// Проверка пересечения прямоугольников
        /// </summary>
        /// <param name="firstLeftX">Координаты по оси X левого нижнего угла первого прямоугольника</param>
        /// <param name="firstLeftY">Координаты по оси Y левого нижнего угла первого прямоугольника</param>
        /// <param name="firstRightX">Координаты по оси X правого верхнего угла первого прямоугольника</param>
        /// <param name="firstRightY">Координаты по оси X правого верхнего угла первого прямоугольника</param>
        /// <param name="secondLeftX">Координаты по оси X левого нижнего угла второго прямоугольника</param>
        /// <param name="secondLeftY">Координаты по оси Y левого нижнего угла второго прямоугольника</param>
        /// <param name="secondRightX">Координаты по оси X правого верхнего угла второго прямоугольника</param>
        /// <param name="secondRightY">Координаты по оси X левого нижнего угла второго прямоугольника</param>
        /// <param name="result">Ожидаемый результат перечсечения</param>
        [Theory]
        [InlineData(1, 1, 10, 10, 0, 0, 2, 2, true)]
        [InlineData(1, 1, 10, 10, 0, 9, 2, 11, true)]
        [InlineData(1, 1, 10, 10, 9, 9, 11, 11, true)]
        [InlineData(1, 1, 10, 10, 9, 0, 11, 9, true)]
        [InlineData(1, 1, 10, 10, 2, 2, 3, 3, true)]
        [InlineData(1, 1, 10, 10, -10, -10, -1, -1, false)]
        public void HasIntersection(double firstLeftX,
                                    double firstLeftY,
                                    double firstRightX,
                                    double firstRightY,
                                    double secondLeftX,
                                    double secondLeftY,
                                    double secondRightX,
                                    double secondRightY,
                                    bool result)
        {
            var firstRectangle = new RectangeWrapper(new Point(firstLeftX, firstLeftY), new Point(firstRightX, firstRightY));
            var secondRectangle = new RectangeWrapper(new Point(secondLeftX, secondLeftY), new Point(secondRightX, secondRightY));
            Assert.Equal(result, RectangleHelpers.Intersects(firstRectangle, secondRectangle));
            Assert.Equal(result, RectangleHelpers.Intersects(secondRectangle, firstRectangle));
        }

    }
}
