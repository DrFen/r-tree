﻿using RTree;
using RTree.GeometryHelpers;
using RTreeTest.Wrappers.AbstractClassWrappers;
using Xunit;

namespace RTreeTest.HelperTests
{
    /// <summary>
    /// Тесты помощника расчёта пересечений
    /// </summary>
    public class IncludeHelperTests
    {
        /// <summary>
        /// Получение прямоугольника
        /// </summary>
        /// <returns></returns>
        private RectangeWrapper GetRectangle()
        {
            return new RectangeWrapper(new Point(1, 1), new Point(10, 10));
        }

        /// <summary>
        /// Проверка нахождения точки на границе
        /// </summary>
        [Fact]
        public void PointInBorder()
        {

            Assert.True(IncludeHelper.PointInNode(new Point(1, 2), GetRectangle()));

            Assert.True(IncludeHelper.PointInNode(new Point(2, 10), GetRectangle()));

            Assert.True(IncludeHelper.PointInNode(new Point(10, 2), GetRectangle()));

            Assert.True(IncludeHelper.PointInNode(new Point(2, 10), GetRectangle()));


        }

        /// <summary>
        /// Проверка нахождения точки за пределами границы по оси X
        /// </summary>
        [Fact]
        public void PointOutOfBorderX()
        {
            Assert.False(IncludeHelper.PointInNode(new Point(0, 2), GetRectangle()));

            Assert.False(IncludeHelper.PointInNode(new Point(11, 2), GetRectangle()));
        }


        /// <summary>
        /// Проверка нахождения точки за пределами границы по оси Y
        /// </summary>
        [Fact]
        public void PointOutOfBorderY()
        {
            Assert.False(IncludeHelper.PointInNode(new Point(2, 0), GetRectangle()));

            Assert.False(IncludeHelper.PointInNode(new Point(2, 11), GetRectangle()));
        }


        /// <summary>
        /// Проверка нахождения точки за пределами внутри прямоугольника
        /// </summary>
        [Fact]
        public void PointInCenter()
        {
            Assert.True(IncludeHelper.PointInNode(new Point(5, 5), GetRectangle()));
        }
    }
}
