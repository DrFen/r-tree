﻿using RTree;
using RTree.GeometryObjects;

namespace RTreeTest.Wrappers.AbstractClassWrappers
{
    /// <summary>
    /// Обёртка над классом прямоугольнка для тестирования защищенных методов
    /// </summary>
    public class RectangeWrapper : Rectangle
    {
        public RectangeWrapper(Point leftLower, Point rightUpper): base(leftLower, rightUpper) { }

        /// <summary>
        /// Обёртка над изменением размера
        /// </summary>
        /// <param name="point"></param>
        public void IncreaseSizeWrapped(Point point)
        {
            IncreaseSize(point);
        }
       
    }
}
