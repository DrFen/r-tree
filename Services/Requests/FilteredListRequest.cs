﻿using Domain.WidgetDomains;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Services.Requests
{
    /// <summary>
    /// Получение списка с фильтрацией по зоне полного вхождения виджетов
    /// </summary>
    public class FilteredListRequest : SimpleListRequest
    {
        /// <summary>
        /// Координаты нижнего левого угла по оси X
        /// </summary>
        [Required]
        [JsonProperty("left_lower_x")]
        public int X { get; set; }

        /// <summary>
        /// Координаты нижнего левого углапо оси Y
        /// </summary>
        [Required]
        [JsonProperty("left_lower_y")]
        public int Y { get; set; }

        /// <summary>
        /// Ширина прямоугольника фильтрации
        /// </summary>
        [Required]
        [Range(1, int.MaxValue)]
        [JsonProperty("width")]
        public int Width { get; set; }

        /// <summary>
        /// Высота прямоугольника фильтрации
        /// </summary>
        [Required]
        [Range(1, int.MaxValue)]
        [JsonProperty("height")]
        public int Height { get; set; }

        /// <summary>
        /// Границы поиска
        /// </summary>
        [JsonIgnore]
        public WidgetRectangle Rectangle => new WidgetRectangle(X, Y, Width, Height);
    }
}
