﻿using Domain.WidgetDomains;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Services.Requests
{
    /// <summary>
    /// Запрос на создание виджета
    /// </summary>
    public class CreateWidgetRequest
    {
        /// <summary>
        /// Координаты нижнего левого угла по оси X
        /// </summary>
        [Required]
        [JsonProperty("left_lower_x")]
        public int X { get; set; }

        /// <summary>
        /// Координаты нижнего левого углапо оси Y
        /// </summary>
        [Required]
        [JsonProperty("left_lower_y")]
        public int Y { get; set; }

        /// <summary>
        /// Ширина виджета
        /// </summary>
        [Required]
        [Range(1, int.MaxValue)]
        [JsonProperty("width")]
        public int Width { get; set; }

        /// <summary>
        /// Высота виджета
        /// </summary>
        [Required]
        [Range(1, int.MaxValue)]
        [JsonProperty("height")]
        public int Height { get; set; }

        /// <summary>
        /// Z-индекс виджета
        /// </summary>
        [JsonProperty("z_index")]
        public int? ZIndex { get; set; }

        /// <summary>
        /// Виджет
        /// </summary>
        [JsonIgnore]
        public Widget Widget => new Widget(X, Y, ZIndex, Width, Height);
    }
}
