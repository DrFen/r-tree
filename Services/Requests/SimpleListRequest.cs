﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Services.Requests
{
    /// <summary>
    /// Простой запрос получения списка
    /// </summary>
    public class SimpleListRequest
    {
        /// <summary>
        /// Количество записей которое необходимо пропустить
        /// </summary>
        [Range(0, int.MaxValue)]
        [JsonProperty("offset")]
        public int? Offset { get; set; }

        /// <summary>
        /// Количество записей для вывода на странице
        /// </summary>
        [Range(1, int.MaxValue)]
        [JsonProperty("page_size")]
        public int? PageSize { get; set; }
    }
}
