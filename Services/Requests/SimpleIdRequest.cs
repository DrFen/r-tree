﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Services.Requests
{
    /// <summary>
    /// Простой запрос действия по идентификатору
    /// </summary>
    public class SimpleIdRequest
    {
        /// <summary>
        /// Иденфитикатор
        /// </summary>
        [Required]
        [JsonProperty("id")]
        public Guid Id { get; set; }
    }
}
