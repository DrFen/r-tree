﻿using Services.Requests;
using Services.Responses;
using System.Collections.Generic;

namespace Services.Interfaces
{
    /// <summary>
    /// Сервис работы с виджетами
    /// </summary>
    public interface IWidgetService
    {
        /// <summary>
        /// Создание виджета
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Полное представление виджета</returns>
        WidgetFullResponse Create(CreateWidgetRequest request);

        /// <summary>
        /// Получение виджета по идентификатору
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Полное представление виджета</returns>
        WidgetFullResponse Get(SimpleIdRequest request);

        /// <summary>
        /// Обновление данных виджета
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Полное представление виджета</returns>
        WidgetFullResponse Update(UpdateWidgetRequest request);

        /// <summary>
        /// Удаление виджета
        /// </summary>
        /// <param name="request"></param>
        void Delete(SimpleIdRequest request);

        /// <summary>
        /// Получение списка, с возможностью пейджинга
        /// </summary>
        /// <returns></returns>
        IEnumerable<WidgetFullResponse> GetSortedList(SimpleListRequest request);

        /// <summary>
        /// Получение списка с фильтрацией по области полного вхождения, с возможностью пейджинга
        /// </summary>
        /// <returns></returns>
        IEnumerable<WidgetFullResponse> GetFilteredList(FilteredListRequest request);

    }
}
