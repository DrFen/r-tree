﻿using Domain.WidgetRepositoryInterfaces;
using Services.Interfaces;
using Services.Requests;
using Services.Responses;
using System.Collections.Generic;
using System.Linq;

namespace Services.Services
{
    /// <summary>
    /// Сервис работы с виджетами
    /// </summary>
    public class WidgetService : IWidgetService
    {
        /// <summary>
        /// Репозиторий работы с виджетами
        /// </summary>
        private readonly IWidgetRepository _repository;

        /// <summary>
        /// Сервис работы с виджетами
        /// </summary>
        /// <param name="repository">Репозиторий работы с виджетами</param>
        public WidgetService(IWidgetRepository repository)
        {
            _repository = repository;
        }


        /// <summary>
        /// Создание виджета
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Полное представление виджета</returns>
        public WidgetFullResponse Create(CreateWidgetRequest request)
        {
            return new WidgetFullResponse(_repository.Insert(request.Widget));
        }

        /// <summary>
        /// Получение виджета по идентификатору
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Полное представление виджета</returns>
        public WidgetFullResponse Get(SimpleIdRequest request)
        {
            return new WidgetFullResponse(_repository.GetById(request.Id));       
        }

        /// <summary>
        /// Обновление данных виджета
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Полное представление виджета</returns>
        public WidgetFullResponse Update(UpdateWidgetRequest request)
        {
            return new WidgetFullResponse(_repository.Update(request.Widget));

        }

        /// <summary>
        /// Удаление виджета
        /// </summary>
        /// <param name="request"></param>
        public void Delete(SimpleIdRequest request)
        {
            _repository.Delete(request.Id);
        }

        /// <summary>
        /// Получение списка, с возможностью пейджинга
        /// </summary>
        /// <returns></returns>
        public IEnumerable<WidgetFullResponse> GetSortedList(SimpleListRequest request)
        {
            return _repository.GetSortedCollection(request?.Offset, request?.PageSize)
                 .Select(s => new WidgetFullResponse(s));
        }

        /// <summary>
        /// Получение списка с фильтрацией по области полного вхождения, с возможностью пейджинга
        /// </summary>
        /// <returns></returns>
        public IEnumerable<WidgetFullResponse> GetFilteredList(FilteredListRequest request)
        {
            return _repository.GetFilteredCollection(request.Rectangle, request.Offset, request.PageSize)
                 .Select(s => new WidgetFullResponse(s));
        }
    }
}
