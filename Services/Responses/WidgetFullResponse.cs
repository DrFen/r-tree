﻿using Domain.WidgetDomains;
using Newtonsoft.Json;
using System;

namespace Services.Responses
{
    /// <summary>
    /// Полное представление виджета
    /// </summary>
    public class WidgetFullResponse
    {
        /// <summary>
        /// Иденфитикатор
        /// </summary>
        [JsonProperty("id")]
        public Guid Id { get; set; }

        /// <summary>
        /// Координаты нижнего левого угла по оси X
        /// </summary>
        [JsonProperty("left_lower_x")]
        public int X { get; set; }

        /// <summary>
        /// Координаты нижнего левого углапо оси Y
        /// </summary>
        [JsonProperty("left_lower_y")]
        public int Y { get; set; }

        /// <summary>
        /// Ширина виджета
        /// </summary>
        [JsonProperty("width")]
        public int Width { get; set; }

        /// <summary>
        /// Высота виджета
        /// </summary>
        [JsonProperty("height")]
        public int Height { get; set; }

        /// <summary>
        /// Z-индекс виджета
        /// </summary>
        [JsonProperty("z_index")]
        public int? ZIndex { get; set; }

        public WidgetFullResponse() { }

        /// <summary>
        /// Полное пользовательское представление виджета
        /// </summary>
        /// <param name="widget"></param>
        public WidgetFullResponse(Widget widget)
        {
            if (widget.Z == null)
                throw new ArgumentNullException();

            Id = widget.Id;
            X = widget.X;
            Y = widget.Y;
            Width = widget.Width;
            Height = widget.Height;
            ZIndex = (int)widget.Z;
        }
    }
}
