﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Services.Requests;
using Services.Responses;

namespace IntegrationTest
{
    public static class SenderAndChecker
    {
        /// <summary>
        /// Отправка запросов на создание
        /// </summary>
        /// <param name="requests"></param>
        /// <param name="server"></param>
        /// <returns></returns>
        public static async Task<ConcurrentBag<WidgetFullResponse>> CheckInsert(List<CreateWidgetRequest> requests, string server)
        {
            var widgets = new ConcurrentBag<WidgetFullResponse>();

            foreach (var request in requests)
            {
                using (var client = new HttpClient())
                {
                    var result = await client.PostAsync($"{server}/widget/create", new JsonContent(request))
                        .ConfigureAwait(false);
                    var content = await result.Content.ReadAsStringAsync();
                    var answer = JsonConvert.DeserializeObject<WidgetFullResponse>(content);

                    if (answer.Width != request.Width
                        || answer.Height != request.Height
                        || answer.X != request.X
                        || answer.Y != request.Y
                        || (request.ZIndex != null && answer.ZIndex != request.ZIndex))
                    {
                        throw new Exception("Искажение данных");
                    }

                    widgets.Add(answer);
                }
            }


            return widgets;
        }

        /// <summary>
        /// Отправка запросов на создание
        /// </summary>
        /// <param name="requests"></param>
        /// <param name="server"></param>
        /// <returns></returns>
        public static async Task<ConcurrentBag<WidgetFullResponse>> CheckUpdate(ConcurrentBag<WidgetFullResponse> requests, string server)
        {
            var widgets = new ConcurrentBag<WidgetFullResponse>();

            var random = new Random();
            foreach (var request in requests)
            {
                var castedRequest = new UpdateWidgetRequest
                {
                    Width = random.Next(1,100),
                    ZIndex = random.Next(1, requests.Count),
                    Height = random.Next(1, 100),
                    Y = random.Next(1, 100),
                    X = random.Next(1, 100),
                    Id = request.Id
                };
                using (var client = new HttpClient())
                {
                    var result = await client
                        .PostAsync($"{server}/widget/update", new JsonContent(castedRequest))
                        .ConfigureAwait(false);
                    var content = await result.Content.ReadAsStringAsync();
                    var answer = JsonConvert.DeserializeObject<WidgetFullResponse>(content);

                    if (answer.Width != castedRequest.Width
                        || answer.Height != castedRequest.Height
                        || answer.X != castedRequest.X
                        || answer.Y != castedRequest.Y
                        || answer.ZIndex != castedRequest.ZIndex)
                    {
                        throw new Exception("Искажение данных");
                    }

                    widgets.Add(answer);
                }
            }


            return widgets;
        }


        /// <summary>
        /// Отправка запросов на чтение
        /// </summary>
        /// <param name="requests"></param>
        /// <param name="server"></param>
        /// <returns></returns>
        public static async Task<ConcurrentBag<WidgetFullResponse>> CheckGet(ConcurrentBag<WidgetFullResponse> requests, string server)
        {
            var widgets = new ConcurrentBag<WidgetFullResponse>();

            foreach (var request in requests)
            {
                var castedRequest = new SimpleIdRequest
                {
                    Id = request.Id
                };
                using (var client = new HttpClient())
                {
                    var result = await client
                        .PostAsync($"{server}/widget/get", new JsonContent(castedRequest))
                        .ConfigureAwait(false);
                    var content = await result.Content.ReadAsStringAsync();
                    var answer = JsonConvert.DeserializeObject<WidgetFullResponse>(content);

                    if (answer.Width != request.Width
                        || answer.Height != request.Height
                        || answer.X != request.X
                        || answer.Y != request.Y
                        || answer.ZIndex != request.ZIndex
                        || answer.Id != request.Id)
                    {
                        throw new Exception("Искажение данных");
                    }
                    widgets.Add(answer);

                }
            }

            return widgets;
        }

    }
}
