﻿using System;

namespace IntegrationTest
{
    class Program
    {
        /// <summary>
        /// Адрес сервера по умолчанию
        /// </summary>
        private static string _serverUrl = "http://localhost:5000";

        private static readonly int _insertCount = 100;

        static void Main()
        {
            Console.WriteLine($"Введите адрес сервера, пустая строка - сервер по-умолчанию ({_serverUrl})");
            var newServer = Console.ReadLine();
            if (!string.IsNullOrWhiteSpace(newServer))
                _serverUrl = newServer;

            Console.WriteLine($"Используется сервер {_serverUrl}");

            Console.WriteLine("Генерируем запросы на вставку");
            var insertRequest = DataGenerator.GetInsertRequests(_insertCount);
            Console.WriteLine($"Сгенерировано {_insertCount} запросов на вставку. Начинаем добавление");

            var widgets = SenderAndChecker.CheckInsert(insertRequest, _serverUrl).GetAwaiter().GetResult();
            Console.WriteLine($"Добавлено {widgets.Count} записей. Начинаем обновление");


            widgets = SenderAndChecker.CheckUpdate(widgets, _serverUrl).GetAwaiter().GetResult();
            Console.WriteLine($"Обновлено {widgets.Count} записей. Начинаем проверку чтения");


            widgets = SenderAndChecker.CheckInsert(DataGenerator.GetInsertRequests(1), _serverUrl).GetAwaiter().GetResult();


            SenderAndChecker.CheckGet(widgets, _serverUrl).GetAwaiter().GetResult();
            Console.WriteLine("Чтение успешно");



            Console.WriteLine("Готово!");





            Console.ReadKey();
        }
    }
}
