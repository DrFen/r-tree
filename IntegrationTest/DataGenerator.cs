﻿using System;
using System.Collections.Generic;
using Services.Requests;

namespace IntegrationTest
{
    public static class DataGenerator
    {
        /// <summary>
        /// Создание запросов на добавление
        /// </summary>
        /// <param name="count">Количество заросов</param>
        /// <param name="delimiterWithZ">Чило должно быть кратно чему, что бы создавался запрос с Z индексом</param>
        /// <returns></returns>
        public static List<CreateWidgetRequest> GetInsertRequests(int count, int delimiterWithZ = 2)
        {
            var random = new Random();
            var result = new List<CreateWidgetRequest>();
            for (var i = 1; i <= count; i++)
            {
                result.Add(new CreateWidgetRequest
                {
                    X = random.Next(1, 100),
                    Y = random.Next(1, 100),
                    Height = random.Next(1, 10),
                    Width = random.Next(1, 10),
                    ZIndex = i % delimiterWithZ == 0 ? random.Next(1, count):(int?)null
                });
            }

            return result;

        }
    }
}
