﻿using RTree.GeometryHelpers;
using RTree.GeometryObjects;

namespace RTree.Extensions
{
    /// <summary>
    /// Расширение класса прямоугольника
    /// </summary>
    public static class RectangleExtensions
    {

        /// <summary>
        /// Прямоугольник содержит точку
        /// </summary>
        /// <param name="rectangle">Прямоугольник</param>
        /// <param name="pointX">Координаты точки по оси X</param>
        /// <param name="pointY">Координаты точки по оси Y</param>
        /// <returns></returns>
        public static bool ContainsPoint(this Rectangle rectangle, double pointX, double pointY)
        {
            return IncludeHelper.PointInNode(pointX, pointY, rectangle);

        }

        /// <summary>
        /// Проверка на пересечение со вторым прямоугольником
        /// </summary>
        /// <param name="firstRectangle"></param>
        /// <param name="secondRectangle"></param>
        /// <returns></returns>
        public static bool Intersects(this Rectangle firstRectangle, Rectangle secondRectangle)
        {
            return RectangleHelpers.Intersects(firstRectangle, secondRectangle);
        }

        /// <summary>
        /// Проверка, что прямоугольник находится внутри дургого прямоугольника
        /// </summary>
        /// <param name="innerRectangle">Внутренний прямоугольник</param>
        /// <param name="externalRectangle">Внешний прямоугольник</param>
        public static bool RectangleInRectangle(this Rectangle innerRectangle, Rectangle externalRectangle)
        {
            return RectangleHelpers.RectangleInRectangle(innerRectangle, externalRectangle);
        }

    }
}
