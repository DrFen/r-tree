﻿using RTree.GeometryHelpers;
using RTree.GeometryObjects;

namespace RTree.Extensions
{
    /// <summary>
    /// Класс расширения точки
    /// </summary>
    public static class PointExtensions
    {
        /// <summary>
        /// Точка находится внутри области
        /// </summary>
        /// <param name="point"></param>
        /// <param name="rectangle"></param>
        /// <param name="includeLeftBorder"></param>
        /// <param name="includeBottomBorder"></param>
        /// <returns></returns>
        public static bool PointInRectangle(this Point point, Rectangle rectangle, bool includeLeftBorder = true, bool includeBottomBorder = true)
        {
            return IncludeHelper.PointInNode(point, rectangle, includeLeftBorder, includeBottomBorder);
        }
    }
}
