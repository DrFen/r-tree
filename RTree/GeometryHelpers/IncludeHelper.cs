﻿using RTree.GeometryObjects;

namespace RTree.GeometryHelpers
{
    /// <summary>
    /// Помощник расчёта пересечений
    /// </summary>
    public static class IncludeHelper
    {
        /// <summary>
        /// Проверка, что точка входит в координаты узла
        /// </summary>
        /// <param name="point">Проверяемая точка</param>
        /// <param name="rectangle">Приямоугольник, в котором проверяем</param>
        /// <param name="includeLeftBorder"></param>
        /// <param name="includeBottomBorder"></param>  /// <returns></returns>
        public static bool PointInNode(Point point, Rectangle rectangle, bool includeLeftBorder = true, bool includeBottomBorder = true)
        {
            return PointInNode(point, rectangle.LeftLower, rectangle.RightUpper, includeLeftBorder, includeBottomBorder);
        }

        /// <summary>
        /// Проверка, что точка входит в прямоугольник между двумя точками
        /// </summary>
        /// <param name="point">Проверяемая точка</param>
        /// <param name="leftLower">Левый нижний угол</param>
        /// <param name="rightUpper">Верхний правый угол</param>
        /// <param name="includeLeftBorder"></param>
        /// <param name="includeBottomBorder"></param>
        /// <returns></returns>
        public static bool PointInNode(Point point, Point leftLower, Point rightUpper, bool includeLeftBorder = true, bool includeBottomBorder = true)
        {
            return PointInNode(point.X, point.Y, leftLower, rightUpper, includeLeftBorder, includeBottomBorder);
        }

        /// <summary>
        /// Проверка, что точка входит в прямоугольник между двумя точками
        /// </summary>
        /// <param name="pointX">Координаты точки по оси X</param>
        /// <param name="pointY">Координаты точки по оси Y</param>
        /// <param name="rectangle">Прямоугольник</param>
        /// <param name="includeLeftBorder"></param>
        /// <param name="includeBottomBorder"></param>

        /// <returns></returns>
        public static bool PointInNode(double pointX,
            double pointY,
            Rectangle rectangle,
            bool includeLeftBorder = true,
            bool includeBottomBorder = true)
        {
            return PointInNode(pointX, pointY, rectangle.LeftLower, rectangle.RightUpper, includeLeftBorder, includeBottomBorder);
        }

        /// <summary>
        /// Проверка, что точка входит в прямоугольник между двумя точками
        /// </summary>
        /// <param name="pointX">Координаты точки по оси X</param>
        /// <param name="pointY">Координаты точки по оси Y</param>
        /// <param name="leftLower">Левый нижний угол</param>
        /// <param name="rightUpper">Верхний правый угол</param>
        /// <param name="includeLeftBorder"></param>
        /// <param name="includeBottomBorder"></param>
        /// <returns></returns>
        public static bool PointInNode(double pointX,
            double pointY,
            Point leftLower,
            Point rightUpper,
            bool includeLeftBorder = true,
            bool includeBottomBorder = true)
        {
            return ((includeLeftBorder && pointX >= leftLower.X) || (!includeLeftBorder && pointX > leftLower.X))
                   && pointX <= rightUpper.X
                   && pointY >= leftLower.Y
                   && ((includeBottomBorder && pointY >= leftLower.Y) || (!includeBottomBorder && pointY > leftLower.Y))
                   && pointY <= rightUpper.Y;
        }
    }
}
