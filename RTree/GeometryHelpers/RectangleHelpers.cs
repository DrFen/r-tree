﻿using RTree.Extensions;
using RTree.GeometryObjects;

namespace RTree.GeometryHelpers
{
    /// <summary>
    /// Помощник работы с прямоугольником
    /// </summary>
    public class RectangleHelpers
    {
        
        /// <summary>
        /// Два прямоугольника пересекаются
        /// </summary>
        /// <param name="firstRectangle"></param>
        /// <param name="secondRectangle"></param>
        /// <returns></returns>
        public static bool Intersects(Rectangle firstRectangle, Rectangle secondRectangle)
        {
            //TODO плохочитаемо, надо подумать как декомпозировать
            return firstRectangle.LeftLower.PointInRectangle(secondRectangle)
                 || firstRectangle.RightUpper.PointInRectangle(secondRectangle)
                 || secondRectangle.ContainsPoint(firstRectangle.LeftUpperX, firstRectangle.LeftUpperY)
                 || secondRectangle.ContainsPoint(firstRectangle.RightLowerX, firstRectangle.RightLowerY)

                 || secondRectangle.LeftLower.PointInRectangle(firstRectangle)
                 || secondRectangle.RightUpper.PointInRectangle(firstRectangle)
                 || firstRectangle.ContainsPoint(secondRectangle.LeftUpperX, secondRectangle.LeftUpperY)
                 || firstRectangle.ContainsPoint(secondRectangle.RightLowerX, secondRectangle.RightLowerY);
        }

        /// <summary>
        /// Проверка, что прямоугольник находится внутри дургого прямоугольника
        /// </summary>
        /// <param name="innerRectangle">Внутренний прямоугольник</param>
        /// <param name="externalRectangle">Внешний прямоугольник</param>
        /// <returns></returns>
        public static bool RectangleInRectangle(Rectangle innerRectangle, Rectangle externalRectangle)
        {
            return externalRectangle.LeftLower.X <= innerRectangle.LeftLower.X
                && externalRectangle.LeftLower.Y <= innerRectangle.LeftLower.Y
                && externalRectangle.RightUpper.X >= innerRectangle.RightUpper.X
                && externalRectangle.RightUpper.Y >= innerRectangle.RightUpper.Y;
        }
    }
}
