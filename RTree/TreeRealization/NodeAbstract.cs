﻿using RTree.GeometryObjects;

namespace RTree.TreeRealization
{
    /// <summary>
    /// Абстрактный узел дерева
    /// </summary>
    public abstract class NodeAbstract<T> : Rectangle
        where T : Point
    {

        /// <summary>
        /// Родительский узел
        /// </summary>
        public NodeAbstract<T> Parent;

        protected NodeAbstract(Point leftLower, Point rightUpper, NodeAbstract<T> parent) : base(leftLower, rightUpper)
        {
            Parent = parent;
        }


        /// <summary>
        /// Изменение размера по цепочке
        /// </summary>
        /// <param name="point"></param>
        protected internal void IncreaseSizeByChain(T point)
        {
            IncreaseSize(point);
            Parent?.IncreaseSizeByChain(point);
        }

        /// <summary>
        /// Добавление точки в узел
        /// </summary>
        /// <param name="point"></param>
        internal abstract void AddPoint(T point);

        /// <summary>
        /// Тип узла
        /// </summary>
        internal abstract NodeType NodeType { get; }

        internal virtual void ReplaceChild(TreeNode<T> node)
        {
            throw new System.NotImplementedException();
        }
    }
}
