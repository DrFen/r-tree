﻿
using System;
using System.Collections.Generic;

namespace RTree.TreeRealization
{
    /// <summary>
    /// Узел дерева с горизонтальным делением детей
    /// </summary>
    public class HorizontalTreeNode<T> : TreeNode<T>
        where T : Point
    {
        public HorizontalTreeNode(NodeAbstract<T> leftNode, NodeAbstract<T> rightNode, NodeAbstract<T> parent, IEnumerable<KeyValuePair<Guid, T>> points) : base(leftNode, rightNode, parent, leftNode.LeftLower.X, points)
        {
        }

        /// <summary>
        /// Расчёт принадлежности точки, входит в левую или правую часть дерева
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        protected internal override bool ToLeft(Point point)
        {
            return point.X <= Delimiter;
        }

    }
}
