﻿using RTree.Extensions;
using RTree.GeometryObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace RTree.TreeRealization
{
    /// <summary>
    /// Виртуальный корень дерева
    /// </summary>
    public class RootNode<T> : NodeAbstract<T>
        where T : Point
    {
        /// <summary>
        /// Де-факто корень дерева
        /// </summary>
        public NodeAbstract<T> Child { get; private set; }


        /// <summary>
        /// Локер
        /// </summary>
        private readonly object _locker = new object();

        private RootNode(LeafNode<T> leafNode) : base(leafNode.LeftLower, leafNode.RightUpper, null)
        {
        }

        /// <summary>
        /// Вид элемента дерева
        /// </summary>
        internal override NodeType NodeType => NodeType.Root;

        /// <summary>
        /// Регистрация корня дерева
        /// </summary>
        /// <param name="leafNode"></param>
        /// <returns></returns>
        public static RootNode<T> Register(LeafNode<T> leafNode)
        {
            var result = new RootNode<T>(leafNode) { Child = leafNode };
            leafNode.Parent = result;
            return result;
        }


        public void InsertPoint(T point)
        {
            lock (_locker)
            {
                AddPoint(point);
            }
        }

        internal override void AddPoint(T point)
        {
            Child.AddPoint(point);
        }


        /// <summary>
        /// Замена ребенка "Листа" на узел
        /// </summary>
        /// <param name="node"></param>
        internal override void ReplaceChild(TreeNode<T> node)
        {
            Child = node;
        }

        /// <summary>
        /// Получение диапазононов дочерних точек входящий в область
        /// </summary>
        /// <param name="rectangle"></param>
        /// <param name="additionalWhereClause">Дополнительное условие отбора при поиске</param>
        /// <returns></returns>
        public List<T> GetPointsInRectangle(Rectangle rectangle, Expression<Func<KeyValuePair<Guid, T>, bool>> additionalWhereClause = null)
        {
            lock (_locker)
            {

                if (rectangle.Intersects(Child))
                {
                    return GetPointsInRectangle(Child, rectangle, additionalWhereClause);
                }

                return new List<T>();
            }
        }

        /// <summary>
        /// Получение диапазононов дочерних точек входящий в область
        /// </summary>
        /// <param name="node">Узел рекурсианого отбора</param>
        /// <param name="rectangle">Прямоугольник поиска</param>
        /// <param name="additionalWhereClause">Дополнительное словие отбора</param>
        /// <returns></returns>
        private List<T> GetPointsInRectangle(NodeAbstract<T> node, Rectangle rectangle, Expression<Func<KeyValuePair<Guid, T>, bool>> additionalWhereClause)
        {
            if (node.NodeType == NodeType.Leaf)
                // ReSharper disable once PossibleNullReferenceException
                return (node as LeafNode<T>).GetPointsInRectangle(rectangle, additionalWhereClause);

            if (node.NodeType == NodeType.TreeNode)
            {
                if (node.RectangleInRectangle(rectangle))
                    // ReSharper disable once PossibleNullReferenceException
                    return (node as TreeNode<T>).Points.Values.ToList();


                var result = new List<T>();

                // ReSharper disable once PossibleNullReferenceException
                (node as TreeNode<T>).GetNodesInRectangle(rectangle)
                    .ForEach(f =>
                    {
                        result.AddRange(GetPointsInRectangle(f, rectangle, additionalWhereClause));
                    });

                return result;
            }


            throw new NotImplementedException();
        }

        /// <summary>
        /// Удаление точки 
        /// </summary>
        /// <param name="point">Координаты точки</param>
        public void DeletePoint(Point point)
        {
            lock (_locker)
            {

                if (!point.PointInRectangle(Child))
                    return;

                DeletePoint(Child, point);

            }
        }

        /// <summary>
        /// Удаление точки в узле
        /// </summary>
        /// <param name="node">Узел дерева</param>
        /// <param name="point">Точка к удалению</param>
        /// <returns></returns>
        private void DeletePoint(NodeAbstract<T> node, Point point)
        {
            if (node.NodeType == NodeType.Leaf)
            {
                // ReSharper disable once PossibleNullReferenceException
                (node as LeafNode<T>).DeletePoint(point.Id);
                return;
            }

            if (node.NodeType == NodeType.TreeNode)
            {
                // ReSharper disable once PossibleNullReferenceException
                (node as TreeNode<T>).DeletePoint(point.Id);
                // ReSharper disable once PossibleNullReferenceException
                DeletePoint((node as TreeNode<T>).GetNodeWithPoint(point), point);
                return;
            }


            throw new NotImplementedException();
        }



    }
}
