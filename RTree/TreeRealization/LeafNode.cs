﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using RTree.GeometryHelpers;
using RTree.GeometryObjects;

[assembly: InternalsVisibleTo("RTreeTest")]
namespace RTree.TreeRealization
{

    /// <summary>
    /// Лист дерева, содержит точки
    /// </summary>
    public class LeafNode<T> : NodeAbstract<T>
        where T : Point
    {
        /// <summary>
        /// Максимальное количество точек в области до деления
        /// </summary>
        private const int PointLimit = 5;

        /// <summary>
        /// Минимальная ширина сегмента
        /// </summary>
        private const int MinimumWidth = 1;


        /// <summary>
        /// Набор точек узла
        /// </summary>
        public ConcurrentDictionary<Guid, T> Points { get; }

        public LeafNode(T leftLower, T rightUpper, NodeAbstract<T> parent, IEnumerable<T> points) : base(leftLower, rightUpper, parent)
        {
            Points = new ConcurrentDictionary<Guid, T>(points.Select(s => new KeyValuePair<Guid, T>(s.Id, s)));
        }

        public LeafNode(Point leftLower, Point rightUpper, NodeAbstract<T> parent, IEnumerable<KeyValuePair<Guid, T>> points) : base(leftLower, rightUpper, parent)
        {
            Points = new ConcurrentDictionary<Guid, T>(points);
        }

        public LeafNode(T point) : base(new Point(point.X, point.Y), new Point(point.X, point.Y), null)
        {
            Points = new ConcurrentDictionary<Guid, T>();
            Points.TryAdd(point.Id, point);
        }
     
        /// <summary>
        /// Тип узла дерева
        /// </summary>
        internal override NodeType NodeType => NodeType.Leaf;


        private LeafNode<T> GetLeafNodeByPoints(Point leftLower, Point rightUpper, bool includeLeftBorder, bool includeBottomBorder)
        {
            return new LeafNode<T>(leftLower, rightUpper, null, Points
                .Where(w => IncludeHelper.PointInNode(w.Value, leftLower, rightUpper, includeLeftBorder, includeBottomBorder)));
        }

        /// <summary>
        /// Разделение узла на 2 с созданием родительского
        /// </summary>
        /// <returns></returns>
        private TreeNode<T> Split()
        {
            var xDelimiter = GetXDelimiter(out var xLeftCount);
            var yDelimiter = GetYDelimiter(out var yLeftCount);


            if (xLeftCount == PointLimit && yLeftCount == PointLimit)
                return null;

            TreeNode<T> result;
            LeafNode<T> leftNode;
            LeafNode<T> rightNode;

            if (Math.Abs(PointLimit - xLeftCount * 2) < Math.Abs(PointLimit - yLeftCount * 2))
            {
                leftNode = GetLeafNodeByPoints(new Point(LeftLower.X, LeftLower.Y),
                    new Point(xDelimiter, RightUpper.Y), true, true);

                rightNode = GetLeafNodeByPoints(new Point(xDelimiter, LeftLower.Y),
                    new Point(RightUpper.X, RightUpper.Y), false, true);

                result = new HorizontalTreeNode<T>(leftNode, rightNode, Parent, Points);

            }
            else
            {
                leftNode = GetLeafNodeByPoints(new Point(LeftLower.X, LeftLower.Y),
                    new Point(RightUpper.X, yDelimiter), true, true);

                rightNode = GetLeafNodeByPoints(new Point(LeftLower.X, yDelimiter),
                    new Point(RightUpper.X, RightUpper.Y), true, false);

                result = new VerticalTreeNode<T>(leftNode, rightNode, Parent, Points);
            }

            leftNode.Parent = result;
            rightNode.Parent = result;

            return result;
        }

        /// <summary>
        /// Общая реализация получения разделителя
        /// </summary>
        /// <param name="startCoordinate">Стартовая координата оси разделения</param>
        /// <param name="groupSelector">Селектор для группировки коллекции при отборе</param>
        /// <param name="leftCount">Количество элементов в левой ветви</param>
        /// <returns></returns>
        private Point GetDelimiter(double startCoordinate, Func<Point, double> groupSelector, out int leftCount)
        {
            //TODO подумать над оптимальностью

            var groupedArray = Points
                .Select(s => s.Value)
                .GroupBy(groupSelector)
                .OrderBy(o => o.Key)
                .Select(s => new { Coordinate = s.Key, PointCount = s.Count() })
                .ToArray();

            leftCount = 0;
            var temporaryCoordinate = 0d;
            double? lockCoordinate = null;
            foreach (var groupedPoints in groupedArray)
            {
                if (lockCoordinate != null && !lockCoordinate.Equals(groupedPoints.Coordinate))
                    return new Point((double)lockCoordinate, (double)lockCoordinate);

                temporaryCoordinate = groupedPoints.Coordinate;
                leftCount += groupedPoints.PointCount;

                if (leftCount >= PointLimit / 2)
                {
                    if (Math.Abs(groupedPoints.Coordinate - startCoordinate) >= MinimumWidth)
                    {
                        return new Point(temporaryCoordinate, temporaryCoordinate);
                    }
                    else
                    {
                        lockCoordinate = temporaryCoordinate + MinimumWidth;
                    }
                }
            }

            leftCount = PointLimit;
            return new Point(temporaryCoordinate, temporaryCoordinate);
        }

        /// <summary>
        /// Получение разделителся по оси X
        /// </summary>
        /// <param name="leftCount"></param>
        /// <returns></returns>
        private double GetXDelimiter(out int leftCount)
        {
            var result = GetDelimiter(LeftLower.X, (point) => point.X, out leftCount);
            return result.X;
        }

        /// <summary>
        /// Получение разделителя по оси Y
        /// </summary>
        /// <param name="leftCount"></param>
        /// <returns></returns>
        private double GetYDelimiter(out int leftCount)
        {
            var result = GetDelimiter(LeftLower.Y, (point) => point.Y, out leftCount);
            return result.Y;
        }


        /// <summary>
        /// Добавление точки в узел
        /// </summary>
        /// <param name="point"></param>
        internal override void AddPoint(T point)
        {

            if (Points.Count >= PointLimit && Width > MinimumWidth)
            {
                var newNode = Split();

                //Не получается непротиворечиво разделить
                if (newNode == null)
                {
                    Points.TryAdd(point.Id, point);
                    IncreaseSizeByChain(point);
                    return;
                }

                Parent?.ReplaceChild(newNode);
                newNode.AddPoint(point);
            }
            else
            {
                Points.TryAdd(point.Id, point);
                IncreaseSizeByChain(point);
            }
        }


        /// <summary>
        /// Получение всех точек листа в заданой области 
        /// среднее O(PointLimit), худшее O(N) когда все точки невозможно разделить
        /// </summary>
        /// <param name="rectangle"></param>
        /// <param name="additionalWhereClause">Дополнительное условие отбора при поиске</param>
        /// <returns></returns>
        internal List<T> GetPointsInRectangle(Rectangle rectangle, Expression<Func<KeyValuePair<Guid, T>, bool>> additionalWhereClause)
        {
            var query = Points.Where(w => IncludeHelper.PointInNode(w.Value, rectangle));

            if (additionalWhereClause != null)
                query = query.AsQueryable().Where(additionalWhereClause);

            return query
            .Select(s => s.Value)
            .ToList();
        }

        internal void DeletePoint(Guid pointId)
        {
            Points.TryRemove(pointId, out var _);
        }

    }
}
