﻿namespace RTree.TreeRealization
{
    /// <summary>
    /// Тип узла
    /// </summary>
    public enum NodeType
    {
        /// <summary>
        /// Корень дерева
        /// </summary>
        Root,

        /// <summary>
        /// Узел дерева не содержащий точек
        /// </summary>
        TreeNode,

        /// <summary>
        /// Лист дерева с точками
        /// </summary>
        Leaf
    }
}
