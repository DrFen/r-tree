﻿using RTree.Extensions;
using RTree.GeometryHelpers;
using RTree.GeometryObjects;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace RTree.TreeRealization
{
    /// <summary>
    /// Узел дерева не содержащий точек
    /// </summary>
    public abstract class TreeNode<T> : NodeAbstract<T>
        where T : Point
    {
        /// <summary>
        /// Координаты оси X по которой происходит деление на дочерние
        /// </summary>
        protected double Delimiter { get; }

        /// <summary>
        /// Левая ветвь дерева с координатой меньше или равным поля 
        /// </summary>
        public NodeAbstract<T> LeftNode;

        /// <summary>
        /// Левая ветвь дерева с координатой строго больше поля 
        /// </summary>
        public NodeAbstract<T> RightNode;

        /// <summary>
        /// Набор точек узла
        /// </summary>
        public ConcurrentDictionary<Guid, T> Points { get; }

        protected TreeNode(NodeAbstract<T> leftNode, NodeAbstract<T> rightNode, NodeAbstract<T> parent, double delimiter, IEnumerable<KeyValuePair<Guid, T>> points) : base(leftNode.LeftLower, rightNode.RightUpper, parent)
        {
            LeftNode = leftNode;
            RightNode = rightNode;
            Delimiter = delimiter;
            Points = new ConcurrentDictionary<Guid, T>(points);
        }

        /// <summary>
        /// Тип узла
        /// </summary>
        internal override NodeType NodeType => NodeType.TreeNode;


        /// <summary>
        /// Относится ли точка к левому узлу
        /// </summary>
        protected internal abstract bool ToLeft(Point point);

        /// <summary>
        /// Замена ребенка "Листа" на узел
        /// </summary>
        /// <param name="node"></param>
        internal override void ReplaceChild(TreeNode<T> node)
        {
            if (ToLeft(node.RightUpper))
            {
                LeftNode = node;
            }
            else
            {
                RightNode = node;
            }

        }

        /// <summary>
        /// Добавление точки в дерево
        /// </summary>
        /// <param name="point"></param>
        internal override void AddPoint(T point)
        {
            Points.TryAdd(point.Id, point);
            GetNodeWithPoint(point).AddPoint(point);
        }

        internal NodeAbstract<T> GetNodeWithPoint(Point point)
        {
            if (IncludeHelper.PointInNode(point, LeftNode))
            {
                return LeftNode;
            }
            else
            {
                return RightNode;
            }
        }

        /// <summary>
        /// Удаление точки
        /// </summary>
        /// <param name="id"></param>
        public void DeletePoint(Guid id)
        {
            Points.TryRemove(id, out _);
        }

        /// <summary>
        /// Получение диапазононов дочерних точек входящий в область
        /// </summary>
        /// <param name="rectangle"></param>
        /// <returns></returns>
        internal List<NodeAbstract<T>> GetNodesInRectangle(Rectangle rectangle)
        {
   
            var result = new List<NodeAbstract<T>>();

            if (rectangle.Intersects(LeftNode))
                result.Add(LeftNode);

            if (rectangle.Intersects(RightNode))
                result.Add(RightNode);


            return result;
        }


    }
}
