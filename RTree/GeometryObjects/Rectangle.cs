﻿using System;

namespace RTree.GeometryObjects
{
    public abstract class Rectangle
    {
        /// <summary>
        /// Координаты нижнего левого угла
        /// </summary>
        public Point LeftLower { get; }

        /// <summary>
        /// Координаты верхнего правого угла
        /// </summary>
        public Point RightUpper { get; }

        /// <summary>
        /// Ширина прямоугольника
        /// </summary>
        public double Width => Math.Abs(LeftLower.X - RightUpper.X);


        /// <summary>
        /// Коорединаты верхней левой точки по оси X
        /// </summary>
        public double LeftUpperX => LeftLower.X;

        /// <summary>
        /// Коорединаты верхней левой точки по оси Y
        /// </summary>
        public double LeftUpperY => RightUpper.Y;

        /// <summary>
        /// Коорединаты нижней правой точки по оси X
        /// </summary>
        public double RightLowerX => RightUpper.X;

        /// <summary>
        /// Коорединаты нижней правой точки по оси Y
        /// </summary>
        public double RightLowerY => LeftLower.Y;


        /// <summary>
        /// Абстрактный узел дерева поиска
        /// </summary>
        /// <param name="leftLower">Левый нижний угол узла</param>
        /// <param name="rightUpper">Правый верхний угол узла</param>
        protected Rectangle(Point leftLower, Point rightUpper)
        {
            LeftLower = leftLower;
            RightUpper = rightUpper;
        }

        /// <summary>
        /// Увеличение размера прямоугольника что бы точка в него входила
        /// </summary>
        /// <param name="point"></param>
        protected void IncreaseSize(Point point)
        {
            //TODO Ну на самом деле, в случае дерева мы можем расшириться только влево/вправо в зависимости от типа узла,
            //но реализуем в общем случае, при большой нагрузке можно упростить
            if (point.X < LeftLower.X || point.X > RightUpper.X)
            {
                if (Math.Abs(RightUpper.X - point.X) > Math.Abs(LeftLower.X - point.X))
                {
                    LeftLower.X = point.X;
                }
                else
                {
                    RightUpper.X = point.X;
                }
            }

            if (point.Y < LeftLower.Y || point.Y > RightUpper.Y)
            {
                if (Math.Abs(RightUpper.Y - point.Y) > Math.Abs(LeftLower.Y - point.Y))
                {
                    LeftLower.Y = point.Y;
                }
                else
                {
                    RightUpper.Y = point.Y;
                }
            }
        }

    }
}
