﻿using System;

namespace RTree
{
    /// <summary>
    /// Точка 
    /// </summary>
    public class Point
    {
        /// <summary>
        /// Идентификатор точки
        /// </summary>
        public Guid Id { get;  }

        /// <summary>
        /// Координаты по оси X
        /// </summary>
        public double X { get; set; }

        /// <summary>
        /// Координаты по оси Y
        /// </summary>
        public double Y { get; set; }


        public Point(Guid id, double x, double y)
        {
            X = x;
            Y = y;
            Id = id;
        }

        internal Point(double x, double y) : this(Guid.NewGuid(), x, y)
        {
        }

        /// <summary>
        /// Клонирование
        /// </summary>
        /// <returns></returns>
        public Point Clone()
        {
            return new Point(Id, X, Y);
        }
    }
}
