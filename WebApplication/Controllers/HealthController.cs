﻿using Microsoft.AspNetCore.Mvc;
using WebApplication.Common;

namespace WebApplication.Controllers
{
    [Route("health")]
    [ApiController]
    public class HealthController : DefaultController
    {
        /// <summary>
        /// Проверка отклика
        /// </summary>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [Route("ping")]
        public IActionResult Ping()
        {
            return InvokeFunction(() => "pong");
        }
    }
}