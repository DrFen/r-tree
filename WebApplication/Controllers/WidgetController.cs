﻿using Microsoft.AspNetCore.Mvc;
using Services.Interfaces;
using Services.Requests;
using WebApplication.Common;

namespace WebApplication.Controllers
{
    /// <summary>
    /// Контроллер работы с виджетами
    /// </summary>
    [Route("widget")]
    [ApiController]
    public class WidgetController : DefaultController
    {
        #region fields
        /// <summary>
        /// Сервис работы с виджетами
        /// </summary>
        private readonly IWidgetService _widgetService;
        #endregion fields

        #region constructor
        /// <summary>
        /// Контроллер работы с виджетами
        /// </summary>
        /// <param name="widgetService"></param>
        public WidgetController(IWidgetService widgetService)
        {
            _widgetService = widgetService;
        }
        #endregion constructor

        #region methods
        /// <summary>
        /// Создание виджета
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Полное представление виджета</returns>
        [HttpPost]
        [Route("create")]
        public IActionResult Create(CreateWidgetRequest request)
        {
            return InvokeFunction(() => _widgetService.Create(request));
        }

        /// <summary>
        /// Получение виджета по идентификатору
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Полное представление виджета</returns>
        [HttpPost]
        [Route("get")]
        public IActionResult Get(SimpleIdRequest request)
        {
            return InvokeFunction(() => _widgetService.Get(request));
        }

        /// <summary>
        /// Обновление данных виджета
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Полное представление виджета</returns>
        [HttpPost]
        [Route("update")]
        public IActionResult Update(UpdateWidgetRequest request)
        {
            return InvokeFunction(() => _widgetService.Update(request));
        }

        /// <summary>
        /// Удаление виджета
        /// </summary>
        /// <param name="request"></param>
        [HttpPost]
        [Route("delete")]
        public IActionResult Delete(SimpleIdRequest request)
        {
            return InvokeAction(() => _widgetService.Delete(request));
        }

        /// <summary>
        /// Получение списка, с возможностью пейджинга
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("sorted-list")]
        public IActionResult GetSortedList(SimpleListRequest request)
        {
            return InvokeFunction(() => _widgetService.GetSortedList(request));
        }

        /// <summary>
        /// Получение списка с фильтрацией по области полного вхождения, с возможностью пейджинга
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("filtered-list")]
        public IActionResult GetFilteredList(FilteredListRequest request)
        {
            return InvokeFunction(() => _widgetService.GetFilteredList(request));
        }
        #endregion methods
    }
}