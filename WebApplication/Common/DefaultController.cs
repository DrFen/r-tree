﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication.Common
{
    public class DefaultController : ControllerBase
    {
        /// <summary>
        /// Выполнение действия  с возвращаемыми данными
        /// </summary>
        /// <param name="action">Делегат метода сервиса</param>
        /// <returns></returns>
        [NonAction]
        public IActionResult InvokeFunction<T>(Func<T> action)
        {
            try
            {
                return Ok(action());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Выполнение действия 
        /// </summary>
        /// <param name="action">Делегат метода сервиса</param>
        /// <returns></returns>
        [NonAction]
        public IActionResult InvokeAction(Action action)
        {
            try
            {
                action();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


    }
}