﻿using Domain.WidgetRepositoryInterfaces;
using InMemory.DAL.RepositoryRealization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using Services.Interfaces;
using Services.Services;

namespace WebApplication
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddSingleton<IWidgetRepository, WidgetRepository>();
            services.AddScoped<IWidgetService, WidgetService>();

            services.AddMvc()
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_0)
            .AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(x => x
           .AllowAnyOrigin()
           .AllowAnyMethod()
           .AllowAnyHeader()
           .AllowCredentials());

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }


            app.UseMvc(routes =>
            {
                routes.MapRoute(
                name: "default",
                template: "{action}/{id?}");

            });

        }
    }
}
