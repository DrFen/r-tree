﻿using RTree;

namespace Domain.WidgetDomains
{
    /// <summary>
    /// Центр виджета
    /// </summary>
    public class WidgetCenterPoint : Point
    {
        /// <summary>
        /// Виджет
        /// </summary>
        public Widget Widget { get; }

        public WidgetCenterPoint(double x, double y, Widget widget) : base(widget.Id, x, y)
        {
            Widget = widget;
        }

    }
}
