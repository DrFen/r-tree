﻿using Common.DAL.Interfaces;
using System;

namespace Domain.WidgetDomains
{
    public class Widget : IEntity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        /// Координаты нижнего левого угла по оси X
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Координаты нижнего левого угла по оси Y
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Z-index
        /// </summary>
        public int? Z { get; set; }

        /// <summary>
        /// Ширина
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// Высота
        /// </summary>
        public int Height { get; set; }


        /// <summary>
        /// Центр
        /// </summary>
        public WidgetCenterPoint Center => new WidgetCenterPoint(X + Width / 2, Y + Height / 2, this);

     
        /// <summary>
        /// Пряиоугольник границ
        /// </summary>
        public WidgetRectangle Rectangle => new WidgetRectangle(X, Y, Width, Height);

        /// <summary>
        /// Создание нового виджета, инстанса которого не сужествует
        /// </summary>
        /// <param name="x">Координаты нижнего левого угла по оси X</param>
        /// <param name="y">Координаты нижнего левого угла по оси Y</param>
        /// <param name="z">Z индекс</param>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота</param>
        public Widget(int x, int y, int? z, int width, int height) : this(Guid.NewGuid(), x, y, z, width, height)
        {

        }


        /// <summary>
        /// Создание виджета, с известным идентификатором
        /// </summary>
        /// <param name="id">Идентфикатор виджета</param>
        /// <param name="x">Координаты нижнего левого угла по оси X</param>
        /// <param name="y">Координаты нижнего левого угла по оси Y</param>
        /// <param name="z">Z индекс</param>
        /// <param name="width">Ширина</param>
        /// <param name="height">Высота</param>
        public Widget(Guid id, int x, int y, int? z, int width, int height)
        {
            X = x;
            Y = y;
            Z = z;
            Width = width;
            Height = height;
            Id = id;
        }

        /// <summary>
        /// Клонирование, чтобы последующие слои не влияли на репозиторий
        /// </summary>
        /// <returns></returns>
        public Widget Clone()
        {
            return new Widget(Id, X, Y, Z, Width, Height);
        }

    }
}
