﻿using RTree;
using RTree.GeometryObjects;
using System;

namespace Domain.WidgetDomains
{
    /// <summary>
    /// Прямоугольник границ виджета
    /// </summary>
    public class WidgetRectangle : Rectangle
    {
        public WidgetRectangle(int x, int y, int width, int height) 
            : base(new Point(Guid.NewGuid(), x,y), new Point(Guid.NewGuid(), x+width, y+height))
        {
        }

        public WidgetRectangle(Point leftLower, Point rightUpper): base(leftLower, rightUpper)
        {

        }
    }
}
