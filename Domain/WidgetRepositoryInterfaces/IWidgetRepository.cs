﻿using Common.DAL.Interfaces;
using Domain.WidgetDomains;
using System.Collections.Generic;

namespace Domain.WidgetRepositoryInterfaces
{
    /// <summary>
    /// Репозиторий виджетов
    /// </summary>
    public interface IWidgetRepository : IRepository<Widget>
    {
        /// <summary>
        /// Получение списка виджетов в области
        /// </summary>
        /// <param name="border">Граница поиска</param>
        /// <param name="offset">Количество записей которые следует пропустить</param>
        /// <param name="pageSize">Размер возвращаемого блока</param>
        /// <returns></returns>
        IEnumerable<Widget> GetFilteredCollection(WidgetRectangle border, int? offset = null, int? pageSize = null);
    }
}
