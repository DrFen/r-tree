﻿using Common.DAL.Exceptions;
using Domain.WidgetDomains;
using Domain.WidgetRepositoryInterfaces;
using RTree;
using RTree.Extensions;
using RTree.TreeRealization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("RepositoryAndServiceTests")]
namespace InMemory.DAL.RepositoryRealization
{
    /// <summary>
    /// Репозиторий работы с виджетами
    /// </summary>
    public class WidgetRepository : IWidgetRepository
    {
        #region fields
        /// <summary>
        /// Коллекция виджетов отсортированная по z индексу
        /// </summary>
        private SortedDictionary<int, Widget> _widgetByZ;

        /// <summary>
        /// Коллекция виджетов по ключу
        /// </summary>
        private Dictionary<Guid, Widget> _widgetByKeys;


        /// <summary>
        /// Дерево центров виджетов
        /// </summary>
        private RootNode<WidgetCenterPoint> _centerTree;

        /// <summary>
        /// Локер
        /// </summary>
        private readonly object _locker = new object();

        /// <summary>
        /// Максимальный текущий Z индекс
        /// </summary>
        private int _maxCurrentZIndex;
        #endregion fields

        #region constructors
        private void Initialize()
        {
            _maxCurrentZIndex = 0;
            _widgetByKeys = new Dictionary<Guid, Widget>();
            _widgetByZ = new SortedDictionary<int, Widget>();
        }

        /// <summary>
        /// Инициализация при необходимости и добавление точки в дерево центров
        /// </summary>
        /// <param name="center"></param>
        private void InsertToTree(WidgetCenterPoint center)
        {
            if (_centerTree == null)
            {
                var leaf = new LeafNode<WidgetCenterPoint>(center);
                _centerTree = RootNode<WidgetCenterPoint>.Register(leaf);
            }
            else
            {
                _centerTree.InsertPoint(center);
            }
        }

        public WidgetRepository()
        {
            Initialize();
        }

        /// <summary>
        /// Для внтуреннего использования, создание с предустановленной коллекцией
        /// </summary>
        /// <param name="widgets"></param>
        internal WidgetRepository(IEnumerable<Widget> widgets)
        {
            var enumerableWidgets = widgets as Widget[] ?? widgets.ToArray();
            if (enumerableWidgets.Any(a => a.Z == null))
                throw new ZIndexMustBeDefinedException();

            Initialize();


            enumerableWidgets.ToList()
                .ForEach(f =>
                {
                    if (f.Z == null)
                        throw new ArgumentNullException();

                    InsertToTree(f.Center);
                    _widgetByKeys.Add(f.Id, f);
                    _widgetByZ.Add((int)f.Z, f);
                    if (_maxCurrentZIndex < (int)f.Z)
                        _maxCurrentZIndex = (int)f.Z;
                });

        }

        /// <summary>
        /// Для внутреннего использования, получение коллекции из словаря "по ключам"
        /// </summary>
        internal List<Widget> FromKeys => _widgetByKeys.Select(s => s.Value).ToList();

        /// <summary>
        /// Для внутреннего использования, получение коллекции из словаря "по z-индесу"
        /// </summary>
        internal List<Widget> FromZIndex => _widgetByZ.Select(s => s.Value).ToList();

        /// <summary>
        /// Для внутреннего использования, получение коллекции из дерева
        /// </summary>
        internal List<Widget> FromTree =>
            _centerTree.GetPointsInRectangle(new WidgetRectangle(new Point(Guid.NewGuid(), int.MinValue, int.MinValue),
                                                                 new Point(Guid.NewGuid(), int.MaxValue, int.MaxValue)))
            .Select(s => s.Widget)
            .ToList();
        #endregion constructors

        /// <summary>
        /// Удаление виджета
        /// </summary>
        /// <param name="id"></param>
        public void Delete(Guid id)
        {
            lock (_locker)
            {
                if (!_widgetByKeys.ContainsKey(id))
                    throw new NoEntityException(id);

                var widget = _widgetByKeys[id];

                if (widget.Z == null)
                    throw new ArgumentNullException();

                if ((int)widget.Z == _maxCurrentZIndex)
                    _maxCurrentZIndex--;

                _widgetByKeys.Remove(id);
                _widgetByZ.Remove((int)widget.Z);
                _centerTree.DeletePoint(widget.Center);
            }
        }

        /// <summary>
        /// Получение виджета по идентификатору
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Widget GetById(Guid id)
        {
            lock (_locker)
            {
                if (!_widgetByKeys.ContainsKey(id))
                    throw new NoEntityException(id);

                return _widgetByKeys[id].Clone();
            }
        }

        /// <summary>
        /// Получение списка виджетов в области
        /// </summary>
        /// <param name="border">Граница поиска</param>
        /// <param name="offset">Количество записей которые следует пропустить</param>
        /// <param name="pageSize">Размер возвращаемого блока</param>
        /// <returns></returns>
        public IEnumerable<Widget> GetFilteredCollection(WidgetRectangle border, int? offset = null, int? pageSize = null)
        {
            lock (_locker)
            {
                if (_centerTree == null)
                    return new Widget[0];

                if (_centerTree.RectangleInRectangle(border))
                    return _widgetByZ.Select(s => s.Value.Clone());

                var result = _centerTree.GetPointsInRectangle(border,
                                                               keyCenter => keyCenter.Value.Widget.Rectangle.RectangleInRectangle(border))
                    .Select(s => s.Widget.Clone())
                    .OrderBy(o => o.Z)
                    .ToArray();

                if ((offset ?? -1) >= 0 && (pageSize ?? -1) > 0)
                    // ReSharper disable once PossibleInvalidOperationException
                    result = result.Skip((int)offset)
                        // ReSharper disable once PossibleInvalidOperationException
                        .Take((int)pageSize)
                        .ToArray();

                return result;
            }
        }

        /// <summary>
        /// Получение отсортированной коллекции виджетов
        /// </summary>
        /// <param name="offset">Количество записей которые следует пропустить</param>
        /// <param name="pageSize">Размер возвращаемого блока</param>
        /// <returns></returns>
        public IEnumerable<Widget> GetSortedCollection(int? offset = null, int? pageSize = null)
        {
            lock (_locker)
            {
                var result = _widgetByZ.Select(s => s.Value.Clone());
                if ((offset ?? -1) >= 0 && (pageSize ?? -1) > 0)
                    result = result
                        // ReSharper disable once PossibleInvalidOperationException
                        .Skip((int)offset)
                        // ReSharper disable once PossibleInvalidOperationException
                        .Take((int)pageSize);

                return result;
            }
        }

        /// <summary>
        /// Добавление виджета
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public Widget Insert(Widget entity)
        {
            lock (_locker)
            {
                if (entity.Z == null)
                {
                    _maxCurrentZIndex++;
                    entity.Z = _maxCurrentZIndex;
                }
                else
                {
                    if (entity.Z > _maxCurrentZIndex)
                    {
                        _maxCurrentZIndex = (int)entity.Z;
                    }
                    else
                    {
                        for (var i = _maxCurrentZIndex; i >= (int)entity.Z; i--)
                        {
                            if (_widgetByZ.Remove(i, out var widget))
                            {
                                widget.Z += 1;
                                _widgetByZ.Add(i + 1, widget);
                            }

                        }

                        _maxCurrentZIndex++;
                    }

                }



                _widgetByKeys.Add(entity.Id, entity);
                _widgetByZ.Add((int)entity.Z, entity);


                InsertToTree(entity.Center);
                return entity.Clone();
            }
        }



        /// <summary>
        /// Обновление виджета
        /// </summary>
        /// <param name="newEntity"></param>
        /// <returns></returns>
        public Widget Update(Widget newEntity)
        {
            lock (_locker)
            {
                if (!_widgetByKeys.ContainsKey(newEntity.Id))
                    throw new NoEntityException(newEntity.Id);

                var oldWidget = _widgetByKeys[newEntity.Id];

                UpdateZ(oldWidget, newEntity);
                UpdateCoordinates(oldWidget, newEntity);

                return newEntity.Clone();
            }
        }

        private void UpdateCoordinates(Widget oldWidget, Widget newWidget)
        {

            var centerWasChanged = oldWidget.Center == newWidget.Center;
            var oldCenter = oldWidget.Center.Clone();

            oldWidget.X = newWidget.X;
            oldWidget.Y = newWidget.Y;
            oldWidget.Width = newWidget.Width;
            oldWidget.Height = newWidget.Height;

            if (!centerWasChanged)
                return;

            _centerTree.DeletePoint(oldCenter);
            _centerTree.InsertPoint(oldWidget.Center);
        }

        private void UpdateZ(Widget oldWidget, Widget newWidget)
        {
            //TODO уточнить так ли это
            if (newWidget.Z == null)
            {
                _maxCurrentZIndex++;
                newWidget.Z = _maxCurrentZIndex;
            }


            if (oldWidget.Z == null)
                throw new ArgumentNullException();

            if (oldWidget.Z == newWidget.Z)
                return;
            _widgetByZ.Remove((int)oldWidget.Z);

            if (_widgetByZ.ContainsKey((int)newWidget.Z))
            {
                if (newWidget.Z < oldWidget.Z)
                {
                    UpdateZInUpper((int)newWidget.Z, (int)oldWidget.Z);
                }
                else
                {
                    UpdateZDown((int)newWidget.Z);
                }
            }

            oldWidget.Z = newWidget.Z;
            _widgetByZ.Add((int)newWidget.Z, oldWidget);

        }

        /// <summary>
        /// Пренос виджетов на уровень выше 
        /// </summary>
        /// <param name="from">С какого уровня необходимо перенести</param>
        /// <param name="to">На какой</param>
        private void UpdateZInUpper(int from, int to)
        {

            for (var i = to; i >= from; i--)
            {
                if (_widgetByZ.Remove(i, out var widget))
                {
                    widget.Z += 1;
                    _widgetByZ.Add(i + 1, widget);
                }

            }

        }

        /// <summary>
        /// Пренос виджетов на уровене ниже 
        /// </summary>
        /// <param name="to">На какой</param>
        private void UpdateZDown(int to)
        {

            for (var i = _maxCurrentZIndex; i >= to; i--)
            {
                if (_widgetByZ.Remove(i, out var widget))
                {
                    widget.Z += 1;
                    _widgetByZ.Add(i + 1, widget);
                }

            }

            _maxCurrentZIndex++;

        }
    }
}
